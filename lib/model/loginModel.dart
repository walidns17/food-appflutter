class LoginModel {
  bool? isSucceeded;
  int? apiStatusCode;
  String? errorMessage;
  ReturnData? returnData;

  LoginModel(
      {this.isSucceeded,
        this.apiStatusCode,
        this.errorMessage,
        this.returnData});

  LoginModel.fromJson(Map<String, dynamic> json) {
    isSucceeded = json['isSucceeded'];
    apiStatusCode = json['apiStatusCode'];
    errorMessage = json['errorMessage'];
    returnData = json['returnData'] != null
        ? new ReturnData.fromJson(json['returnData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isSucceeded'] = this.isSucceeded;
    data['apiStatusCode'] = this.apiStatusCode;
    data['errorMessage'] = this.errorMessage;
    if (this.returnData != null) {
      data['returnData'] = this.returnData!.toJson();
    }
    return data;
  }
}

class ReturnData {
  String? token;
  Null? refreshToken;
  String? expiration;

  ReturnData({this.token, this.refreshToken, this.expiration});

  ReturnData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    refreshToken = json['refreshToken'];
    expiration = json['expiration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['refreshToken'] = this.refreshToken;
    data['expiration'] = this.expiration;
    return data;
  }
}