
class Cart {
  String title;
  String images;
  int quantity;
  double price;
  int productDetailsSizeId;
  List<AdditionalComponentCart> additionalComponentCarts;
  List<SubAdditionalComponentCart> subAdditionalComponentCarts;

  Cart({
    required this.title,
    required this.images,
    required this.quantity,
    required this.price,

    required this.productDetailsSizeId,
    required this.additionalComponentCarts,
    required this.subAdditionalComponentCarts,
  });

  void updateData(Cart item) {}
}

class AdditionalComponentCart {
  int quantity;
  int additionalCompnentCartId;

  AdditionalComponentCart({
    required this.quantity,
    required this.additionalCompnentCartId,
  });
}

class SubAdditionalComponentCart {
  int quantity;
  int subAdditionalCompnentCartId;

  SubAdditionalComponentCart({
    required this.quantity,
    required this.subAdditionalCompnentCartId,
  });
}
