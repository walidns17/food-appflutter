class ResponseBoolModel {
  bool? isSucceeded;
  int? apiStatusCode;
  String? errorMessage;
  bool? returnData;

  ResponseBoolModel(
      {this.isSucceeded,
        this.apiStatusCode,
        this.errorMessage,
        this.returnData});

  ResponseBoolModel.fromJson(Map<String, dynamic> json) {
    isSucceeded = json['isSucceeded'];
    apiStatusCode = json['apiStatusCode'];
    errorMessage = json['errorMessage'];
    returnData = json['returnData'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isSucceeded'] = this.isSucceeded;
    data['apiStatusCode'] = this.apiStatusCode;
    data['errorMessage'] = this.errorMessage;
    data['returnData'] = this.returnData;
    return data;
  }
}