import 'dart:ffi';

class Popular {
  bool? isSucceeded;
  int? apiStatusCode;
  String? errorMessage;
  List<ReturnData>? returnData;

  Popular(
      {this.isSucceeded,
        this.apiStatusCode,
        this.errorMessage,
        this.returnData});

  Popular.fromJson(Map<String, dynamic> json) {
    isSucceeded = json['isSucceeded'];
    apiStatusCode = json['apiStatusCode'];
    errorMessage = json['errorMessage'];
    if (json['returnData'] != null) {
      returnData = <ReturnData>[];
      json['returnData'].forEach((v) {
        returnData!.add(ReturnData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['isSucceeded'] = this.isSucceeded;
    data['apiStatusCode'] = this.apiStatusCode;
    data['errorMessage'] = this.errorMessage;
    if (this.returnData != null) {
      data['returnData'] = this.returnData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReturnData {
  int? shopId;
  String? shopName;
  String? shopNameAr;
  String? avatar;
  String? headerImage;
  String? shopDescriptionAr;
  String? shopDescription;
  int? orderCount;
  double? shopeRate;

  ReturnData(
      {this.shopId,
        this.shopName,
        this.shopNameAr,
        this.avatar,
        this.headerImage,
        this.shopDescriptionAr,
        this.shopDescription,
        this.orderCount,
        this.shopeRate});

  ReturnData.fromJson(Map<String, dynamic> json) {
    shopId = json['shopId'];
    shopName = json['shopName'];
    shopNameAr = json['shopNameAr'];
    avatar = json['avatar'];
    headerImage = json['headerImage'];
    shopDescriptionAr = json['shopDescriptionAr'];
    shopDescription = json['shopDescription'];
    orderCount = json['orderCount'];
    shopeRate = json['shopeRate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['shopId'] = this.shopId;
    data['shopName'] = this.shopName;
    data['shopNameAr'] = this.shopNameAr;
    data['avatar'] = this.avatar;
    data['headerImage'] = this.headerImage;
    data['shopDescriptionAr'] = this.shopDescriptionAr;
    data['shopDescription'] = this.shopDescription;
    data['orderCount'] = this.orderCount;
    data['shopeRate'] = this.shopeRate;
    return data;
  }
}