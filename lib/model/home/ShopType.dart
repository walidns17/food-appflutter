/// isSucceeded : true
/// apiStatusCode : 200
/// errorMessage : ""
/// returnData : [{"id":13,"type":"Fast Food","typeAr":"وجبات سريعة","description":"Fast Food","descriptionAr":"وجبات سريعة","createAt":"2022-01-19T21:52:44.5856512","updateAt":"2023-07-31T11:09:25.269984","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b52213ce-ed5e-4c4f-9692-9eff7e491f82/fasterImage/481.png","color":"#1c782b","isActive":true,"sort":0},{"id":14,"type":"Coffee","typeAr":"قهوة","description":"Coffee Lovers","descriptionAr":"لعشاق القهوة ","createAt":"2022-01-19T21:58:46.8950207","updateAt":"2023-08-03T11:07:06.3787486","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/ba7b775c-a0b4-4aa5-8be6-ab1f4eed9496/fasterImage/84.png","color":"#c24242","isActive":true,"sort":0},{"id":15,"type":"Khaleeji ","typeAr":"خليجي","description":"Saudi Food","descriptionAr":"اكلات سعودية","createAt":"2022-01-20T14:26:34.2164899","updateAt":"2023-08-03T11:07:49.6211417","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/e440dd41-d911-42cf-a572-d09b7ebc0e58/fasterImage/805.png","color":"#e918ec","isActive":true,"sort":0},{"id":17,"type":"Delivery Offers","typeAr":"عروض التوصيل","description":"Best Daily Offers","descriptionAr":"عروض متنوعة ومتجددة ","createAt":"2022-01-20T15:54:59.9233777","updateAt":"2023-07-31T11:55:27.2921159","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/02e2e12e-8f8c-4e1c-867a-31f2fa3ee2dd/fasterImage/721.png","color":"#ed5e5e","isActive":true,"sort":0},{"id":18,"type":"Market and Appliances","typeAr":"ماركت و احتياجات","description":"Market and Appliances","descriptionAr":" بقالية و احتياجات منزلية","createAt":"2022-01-20T15:56:24.9593072","updateAt":"2023-03-25T16:03:54.6759783","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/3fc3e668-2596-4b3a-9d77-3e7ad826b55b/burger.png","color":"#c84646","isActive":false,"sort":0},{"id":19,"type":"Pharmacy And Care ","typeAr":"صيدليات ","description":"Medicine health","descriptionAr":"دواء علاج صحة","createAt":"2022-01-20T16:10:48.4106321","updateAt":"2023-01-02T12:11:42.7796993","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/85ca347f-a8f5-4d3d-a1fe-ba95a9e72bc3/burger11.png","color":"#a45151","isActive":false,"sort":0},{"id":20,"type":"Beauty and Health","typeAr":"جمال و صحة","description":"Beauty and Health Care","descriptionAr":"منتجات الجمال و الرعاية الشخصية","createAt":"2022-01-20T16:16:08.7620598","updateAt":"2022-11-09T13:27:16.0738132","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/398653fa-3fbf-4e1b-8244-f85e8f3122ef/burger1.png","color":"#e85454","isActive":false,"sort":0},{"id":21,"type":"Healthy","typeAr":"صحي","description":"Healthy and Fresh Juices ","descriptionAr":"صحي وعصائر","createAt":"2022-02-14T23:52:47.8742281","updateAt":"2023-07-31T11:54:49.5813058","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/f07a6c6c-6575-48ea-93bd-4806dfa95fba/fasterImage/65.png","color":"#d2de2b","isActive":true,"sort":0},{"id":24,"type":"n ","typeAr":" ف","description":"non active","descriptionAr":"محل متوقف","createAt":"2022-08-30T12:25:54.8561114","updateAt":"2022-10-06T11:18:16.0696674","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/70ee5dd4-977c-421a-b467-40536611eeae/images.png","color":"#956565","isActive":false,"sort":0},{"id":25,"type":" Dessert","typeAr":"حلويات","description":"Sweet","descriptionAr":"حلويات","createAt":"2022-09-01T12:36:30.3253576","updateAt":"2023-07-31T11:56:58.6658303","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/5c4a0dc9-f948-4eac-a3fc-36e4b8a13d2f/fasterImage/312.png","color":"#781c1c","isActive":true,"sort":0},{"id":26,"type":"fried","typeAr":"مقلي","description":"test","descriptionAr":"اختبار","createAt":"2023-07-25T15:04:47.2228735","updateAt":"2023-07-25T15:07:43.8955786","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/a00314f1-5ba2-4de8-a459-49ec6d095ea7/fasterImage/667.png","color":"#843e3e","isActive":false,"sort":0},{"id":27,"type":"Italian","typeAr":"إيطالي","description":"Pasta, Pizza","descriptionAr":"باستا بيتزا","createAt":"2023-08-07T13:00:35.3864705","updateAt":"2023-08-07T13:00:35.3947116","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/6cf82be0-d651-4d34-89c8-cf5b44f46502/fasterImage/433.png","color":"#131010","isActive":false,"sort":0},{"id":28,"type":"Middle Eastern","typeAr":"شرقي","description":"Shami, Egyptian","descriptionAr":"شامي مصري","createAt":"2023-08-07T13:03:05.4938994","updateAt":"2023-08-07T13:03:05.4939655","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/cc2c8418-f021-41f3-a415-9f3b672a5525/fasterImage/544.jpeg","color":"#3f1717","isActive":false,"sort":0},{"id":29,"type":"Asian","typeAr":"آسيوي","description":"Noodles, Sushi","descriptionAr":"نودلز شاورما","createAt":"2023-08-07T13:04:31.0007657","updateAt":"2023-08-07T13:04:31.000844","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/295e4bab-3544-4c26-88c2-2a327aba1183/fasterImage/514.png","color":"#731717","isActive":false,"sort":0},{"id":30,"type":"Indian","typeAr":"هندي","description":"Biriyani","descriptionAr":"برياني","createAt":"2023-08-07T13:06:05.528161","updateAt":"2023-08-07T13:06:05.5282368","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/e7a846a2-9d45-4fb9-874e-46ea7c246e46/fasterImage/562.png","color":"#6e1212","isActive":false,"sort":0},{"id":31,"type":"Bakery","typeAr":"مخبوزات","description":"Bread","descriptionAr":"خبز","createAt":"2023-08-07T13:07:08.8515895","updateAt":"2023-08-07T13:07:08.8516335","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/aec1c9c8-8b41-45c2-8df6-863096da5152/fasterImage/754.png","color":"#ab3636","isActive":false,"sort":0},{"id":32,"type":"Seafood","typeAr":"بحريات","description":"Fish","descriptionAr":"سمك","createAt":"2023-08-07T13:08:11.8449386","updateAt":"2023-08-07T13:08:11.8449656","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/99687d6b-cbe0-40fa-89e3-c99616a42284/fasterImage/896.png","color":"#672323","isActive":false,"sort":0},{"id":33,"type":"Grill","typeAr":"مشويات","description":"Grills","descriptionAr":"مشاوي","createAt":"2023-08-07T13:09:02.5210582","updateAt":"2023-08-07T13:09:02.5211144","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/7004508e-4dda-4a4f-9b9c-904ee8453730/fasterImage/76.png","color":"#7c4141","isActive":false,"sort":0},{"id":34,"type":"Breakfast","typeAr":"فطور","description":"Breakfast","descriptionAr":"فطور","createAt":"2023-08-07T13:10:06.1638063","updateAt":"2023-08-07T13:10:06.1638399","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/86ae08bc-8925-4a73-8381-87214b7828c3/fasterImage/863.png","color":"#ab4444","isActive":false,"sort":0},{"id":35,"type":"Roastery","typeAr":"محمصة","description":"Coffee beans & nuts","descriptionAr":"حبوب قهوة و مكسرات","createAt":"2023-08-07T13:11:16.2877362","updateAt":"2023-08-07T13:11:16.2877641","shopTypeAvatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/68c4b16a-46c1-4afe-932f-baeff5d03d90/fasterImage/896.png","color":"#6f2525","isActive":false,"sort":0}]

class ShopType {
  ShopType({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      List<ReturnData>? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  ShopType.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    if (json['returnData'] != null) {
      _returnData = [];
      json['returnData'].forEach((v) {
        _returnData?.add(ReturnData.fromJson(v));
      });
    }
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  List<ReturnData>? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  List<ReturnData>? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 13
/// type : "Fast Food"
/// typeAr : "وجبات سريعة"
/// description : "Fast Food"
/// descriptionAr : "وجبات سريعة"
/// createAt : "2022-01-19T21:52:44.5856512"
/// updateAt : "2023-07-31T11:09:25.269984"
/// shopTypeAvatar : "https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b52213ce-ed5e-4c4f-9692-9eff7e491f82/fasterImage/481.png"
/// color : "#1c782b"
/// isActive : true
/// sort : 0

class ReturnData {
  ReturnData({
      int? id,
      String? type, 
      String? typeAr, 
      String? description, 
      String? descriptionAr, 
      String? createAt, 
      String? updateAt, 
      String? shopTypeAvatar, 
      String? color, 
      bool? isActive, 
      int? sort,}){
    _id = id;
    _type = type;
    _typeAr = typeAr;
    _description = description;
    _descriptionAr = descriptionAr;
    _createAt = createAt;
    _updateAt = updateAt;
    _shopTypeAvatar = shopTypeAvatar;
    _color = color;
    _isActive = isActive;
    _sort = sort;
}

  ReturnData.fromJson(dynamic json) {
    _id = json['id'];
    _type = json['type'];
    _typeAr = json['typeAr'];
    _description = json['description'];
    _descriptionAr = json['descriptionAr'];
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
    _shopTypeAvatar = json['shopTypeAvatar'];
    _color = json['color'];
    _isActive = json['isActive'];
    _sort = json['sort'];
  }
  int? _id;
  String? _type;
  String? _typeAr;
  String? _description;
  String? _descriptionAr;
  String? _createAt;
  String? _updateAt;
  String? _shopTypeAvatar;
  String? _color;
  bool? _isActive;
  int? _sort;

  int? get id => _id;
  String? get type => _type;
  String? get typeAr => _typeAr;
  String? get description => _description;
  String? get descriptionAr => _descriptionAr;
  String? get createAt => _createAt;
  String? get updateAt => _updateAt;
  String? get shopTypeAvatar => _shopTypeAvatar;
  String? get color => _color;
  bool? get isActive => _isActive;
  int? get sort => _sort;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['type'] = _type;
    map['typeAr'] = _typeAr;
    map['description'] = _description;
    map['descriptionAr'] = _descriptionAr;
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    map['shopTypeAvatar'] = _shopTypeAvatar;
    map['color'] = _color;
    map['isActive'] = _isActive;
    map['sort'] = _sort;
    return map;
  }

}