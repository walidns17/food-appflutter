/// isSucceeded : true
/// apiStatusCode : 200
/// errorMessage : ""
/// returnData : [{"id":6,"foodTypeName":"Juicy","foodTypeNameAr":"جوسي","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b76b90f0-28bc-44d7-89bc-16a4fa81c1cd/fasterImage/459.png","isActive":true,"createAt":"2022-11-03T15:55:30.2834727","updateAt":"2023-08-06T12:25:53.2633505"},{"id":7,"foodTypeName":"Cheesy","foodTypeNameAr":"تشيزي","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/196b7fdf-2532-4e60-9e13-91baadaa1f57/fasterImage/678.png","isActive":true,"createAt":"2022-11-05T09:25:31.4064187","updateAt":"2023-08-06T12:26:32.2228914"},{"id":9,"foodTypeName":"Creamy","foodTypeNameAr":"كريمي","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/eaedf79e-b2ee-4e01-a684-88336f1a747b/fasterImage/755.png","isActive":true,"createAt":"2022-11-10T08:17:59.0674524","updateAt":"2023-08-06T12:40:29.0550373"},{"id":10,"foodTypeName":"foul & 3adas","foodTypeNameAr":"بوفيه و فخاريات","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/f0086cad-75ba-410c-9eda-4be3c994180a/fasterImage/542.png","isActive":true,"createAt":"2022-11-10T08:50:27.1243702","updateAt":"2023-08-06T12:42:50.6167299"},{"id":11,"foodTypeName":"Saucy","foodTypeNameAr":"لغاويص","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/c98f011f-1c5c-41e1-a74e-8be747f921bc/fasterImage/973.png","isActive":true,"createAt":"2022-11-12T11:35:43.4893804","updateAt":"2023-08-06T12:34:08.1305935"},{"id":12,"foodTypeName":"Gatherings","foodTypeNameAr":"أكل الجمعات","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/3b39e3a9-0a40-4032-b4f2-1c9aad5b2116/fasterImage/635.png","isActive":true,"createAt":"2022-11-12T11:37:15.8988359","updateAt":"2023-08-06T12:43:17.538922"},{"id":13,"foodTypeName":"Seafood","foodTypeNameAr":"ماكولات بحرية","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/ee0b5c14-77c0-40d5-a78d-dc35aa8e5538/fasterImage/372.png","isActive":true,"createAt":"2022-11-12T12:19:04.9695599","updateAt":"2023-08-15T12:26:35.541995"},{"id":14,"foodTypeName":"Snacks","foodTypeNameAr":"نقنقة و مفرحات","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/5bb307ec-028f-44d9-b7b1-95c567f4b9e0/fasterImage/114.png","isActive":false,"createAt":"2022-11-12T12:19:41.9076365","updateAt":"2023-08-21T16:32:11.1072142"},{"id":15,"foodTypeName":"Sour","foodTypeNameAr":"ناطع","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/2b25fd70-f423-4807-8907-d97a79d6f7e9/fasterImage/637.png","isActive":true,"createAt":"2022-11-12T12:20:15.3675519","updateAt":"2023-08-06T12:45:10.7496149"},{"id":16,"foodTypeName":"Sweet Tooth","foodTypeNameAr":"حلى يا حلى","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/bcff569a-917c-4a98-9872-8ded05068bf5/fasterImage/947.png","isActive":true,"createAt":"2022-11-12T12:22:11.474196","updateAt":"2023-08-06T12:47:17.7249783"},{"id":18,"foodTypeName":"Spicy","foodTypeNameAr":"سبايسي","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b9e40a56-1104-4ffe-a1e7-e4ccb2101de6/fasterImage/346.png","isActive":true,"createAt":"2022-12-09T15:08:49.0211804","updateAt":"2023-08-06T12:45:39.4810542"},{"id":19,"foodTypeName":"Shawarma","foodTypeNameAr":"شاورمـا","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b3269830-8729-41da-8aac-06eff04eab8f/fasterImage/856.png","isActive":true,"createAt":"2022-12-22T16:30:26.6570223","updateAt":"2023-08-06T12:27:49.4193839"},{"id":20,"foodTypeName":"Light Bites","foodTypeNameAr":"خفايف","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/aa42e9fc-a89a-4125-976b-b79d1f4e8157/fasterImage/757.png","isActive":true,"createAt":"2022-12-22T16:31:08.4196293","updateAt":"2023-08-15T12:52:52.8194646"},{"id":21,"foodTypeName":"Wake me Up","foodTypeNameAr":"صحصحني","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/3c40b225-52b8-4b16-9da1-c0c57b843534/fasterImage/628.png","isActive":true,"createAt":"2022-12-22T16:31:43.0641476","updateAt":"2023-08-06T12:35:39.9865039"},{"id":22,"foodTypeName":"Khaleeji","foodTypeNameAr":"رزيزة","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/e8d7b76b-71a2-47dd-9b1b-0ca8e26cbe83/fasterImage/457.png","isActive":true,"createAt":"2022-12-22T16:32:14.645677","updateAt":"2023-08-06T12:36:14.8210497"},{"id":23,"foodTypeName":"Fresh","foodTypeNameAr":"فريش","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/cbed4db2-f294-44a4-8005-0751020e8ac8/fasterImage/401.png","isActive":true,"createAt":"2022-12-22T16:32:53.7588549","updateAt":"2023-08-15T12:51:38.8249071"},{"id":24,"foodTypeName":"Sweet & Sour","foodTypeNameAr":"حامض و حلو","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/40620e58-389a-4b65-be1a-d8d7d84b7ca5/fasterImage/314.png","isActive":true,"createAt":"2022-12-22T16:33:47.2121493","updateAt":"2023-08-06T12:32:20.7338067"},{"id":25,"foodTypeName":"Icy ","foodTypeNameAr":"يسرسح","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/240e2f56-dcfb-4305-b986-1b897370ca27/fasterImage/712.png","isActive":true,"createAt":"2022-12-22T16:36:01.2576343","updateAt":"2023-08-06T12:38:37.2714472"},{"id":26,"foodTypeName":"Crispy","foodTypeNameAr":"مقرمش","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/4350fe15-fc5a-46a6-a240-5c86b8c98eb9/fasterImage/580.png","isActive":true,"createAt":"2022-12-22T16:36:26.9699654","updateAt":"2023-08-06T12:39:54.9143794"},{"id":27,"foodTypeName":"test","foodTypeNameAr":"تيست","avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/6d6c700e-7918-4080-94db-2be556b4fb25/fasterImage/196.jpeg","isActive":false,"createAt":"2023-07-20T10:35:25.7505184","updateAt":"2023-08-06T12:48:02.8125112"}]

class FoodType {
  FoodType({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      List<ReturnData>? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  FoodType.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    if (json['returnData'] != null) {
      _returnData = [];
      json['returnData'].forEach((v) {
        _returnData?.add(ReturnData.fromJson(v));
      });
    }
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  List<ReturnData>? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  List<ReturnData>? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 6
/// foodTypeName : "Juicy"
/// foodTypeNameAr : "جوسي"
/// avatar : "https://fastersaimages.s3.eu-central-1.amazonaws.com/All/b76b90f0-28bc-44d7-89bc-16a4fa81c1cd/fasterImage/459.png"
/// isActive : true
/// createAt : "2022-11-03T15:55:30.2834727"
/// updateAt : "2023-08-06T12:25:53.2633505"

class ReturnData {
  ReturnData({
      int? id, 
      String? foodTypeName, 
      String? foodTypeNameAr, 
      String? avatar, 
      bool? isActive, 
      String? createAt, 
      String? updateAt,}){
    _id = id;
    _foodTypeName = foodTypeName;
    _foodTypeNameAr = foodTypeNameAr;
    _avatar = avatar;
    _isActive = isActive;
    _createAt = createAt;
    _updateAt = updateAt;
}

  ReturnData.fromJson(dynamic json) {
    _id = json['id'];
    _foodTypeName = json['foodTypeName'];
    _foodTypeNameAr = json['foodTypeNameAr'];
    _avatar = json['avatar'];
    _isActive = json['isActive'];
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
  }
  int? _id;
  String? _foodTypeName;
  String? _foodTypeNameAr;
  String? _avatar;
  bool? _isActive;
  String? _createAt;
  String? _updateAt;

  int? get id => _id;
  String? get foodTypeName => _foodTypeName;
  String? get foodTypeNameAr => _foodTypeNameAr;
  String? get avatar => _avatar;
  bool? get isActive => _isActive;
  String? get createAt => _createAt;
  String? get updateAt => _updateAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['foodTypeName'] = _foodTypeName;
    map['foodTypeNameAr'] = _foodTypeNameAr;
    map['avatar'] = _avatar;
    map['isActive'] = _isActive;
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    return map;
  }

}