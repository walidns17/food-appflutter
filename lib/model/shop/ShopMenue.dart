
class ShopMenue {
  ShopMenue({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      List<ReturnData>? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  ShopMenue.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    if (json['returnData'] != null) {
      _returnData = [];
      json['returnData'].forEach((v) {
        _returnData?.add(ReturnData.fromJson(v));
      });
    }
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  List<ReturnData>? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  List<ReturnData>? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 110
/// productName : "Meat "
/// productNameAr : "اللحم"
/// imagePath : "http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/mac60-0Copy.png"
/// isOffer : false
/// isActive : true
/// deliverectMenuId : null
/// shopId : 36
/// productDetails : [{"id":190,"title":"Big Mac","titleAr":"بيج ماك لحم","price":18,"lastPrice":18,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/mac6.png","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:09:39.2534297","shopId":36},{"id":191,"title":"Big Tasty","titleAr":"بيج تايستي","price":23,"lastPrice":23,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Big0Tasty.png","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:07:45.7925987","shopId":36},{"id":192,"title":"McRoyale","titleAr":"ماك رويال","price":20,"lastPrice":20,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/McRoyale.png","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:11:20.370227","shopId":36},{"id":193,"title":"Quarter Pounder With Cheese","titleAr":"كوارتر باوندر بالجبنة","price":19,"lastPrice":19,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Quarter0Pounder.png","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":null,"snoozeStart":null,"updateAt":"2022-10-23T16:41:17.5598217","shopId":36},{"id":194,"title":"Cheeseburger","titleAr":"تشيز برجر","price":7,"lastPrice":7,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/c2c14d60-5be1-45b5-a878-e64e36437462/fasterImage/918.jpeg","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:18:06.9435077","shopId":36},{"id":5199,"title":"Beefburger ","titleAr":" ييف برغر","price":6,"lastPrice":6,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/fc5bf24a-a79b-4d0b-a483-1f977f5d2fa8/fasterImage/374.jpg","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:20:24.6007108","shopId":36},{"id":5200,"title":"Triple Cheesebuger ","titleAr":"تربل تشيزبرجر","price":15,"lastPrice":15,"note":"0","noteAr":"0","healthInformation":"451","healthInformationAr":"451","coverImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/aeea7794-d64e-4a14-bb7f-6c5b192e84e1/fasterImage/53.jpg","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:25:06.5825431","shopId":36},{"id":5202,"title":"Big Tasty Mushroom ","titleAr":"بيج تستي ماشروم ","price":24,"lastPrice":24,"note":"0","noteAr":"0","healthInformation":"514","healthInformationAr":"514","coverImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/f9af69ad-e96a-45f4-880b-53afaa325d7d/fasterImage/728.jpg","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:27:22.0284922","shopId":36},{"id":5206,"title":"Double Cheese Burger","titleAr":"دبل تشيز برقر ","price":11,"lastPrice":11,"note":"0","noteAr":"0","healthInformation":"0","healthInformationAr":"0","coverImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/0641cb83-7ffc-4197-9e85-ecacd04b93e3/fasterImage/754.jpeg","isActive":true,"availableFromHour":"00:01","availableToHour":"23:59","snoozeEnd":"1970-01-01T00:00:00.000Z","snoozeStart":"1970-01-01T00:00:00.000Z","updateAt":"2023-07-24T08:15:02.37138","shopId":36}]

class ReturnData {
  ReturnData({
      int? id, 
      String? productName, 
      String? productNameAr, 
      String? imagePath, 
      bool? isOffer, 
      bool? isActive, 
      dynamic deliverectMenuId, 
      int? shopId, 
      List<ProductDetails>? productDetails,}){
    _id = id;
    _productName = productName;
    _productNameAr = productNameAr;
    _imagePath = imagePath;
    _isOffer = isOffer;
    _isActive = isActive;
    _deliverectMenuId = deliverectMenuId;
    _shopId = shopId;
    _productDetails = productDetails;
}

  ReturnData.fromJson(dynamic json) {
    _id = json['id'];
    _productName = json['productName'];
    _productNameAr = json['productNameAr'];
    _imagePath = json['imagePath'];
    _isOffer = json['isOffer'];
    _isActive = json['isActive'];
    _deliverectMenuId = json['deliverectMenuId'];
    _shopId = json['shopId'];
    if (json['productDetails'] != null) {
      _productDetails = [];
      json['productDetails'].forEach((v) {
        _productDetails?.add(ProductDetails.fromJson(v));
      });
    }
  }
  int? _id;
  String? _productName;
  String? _productNameAr;
  String? _imagePath;
  bool? _isOffer;
  bool? _isActive;
  dynamic _deliverectMenuId;
  int? _shopId;
  List<ProductDetails>? _productDetails;

  int? get id => _id;
  String? get productName => _productName;
  String? get productNameAr => _productNameAr;
  String? get imagePath => _imagePath;
  bool? get isOffer => _isOffer;
  bool? get isActive => _isActive;
  dynamic get deliverectMenuId => _deliverectMenuId;
  int? get shopId => _shopId;
  List<ProductDetails>? get productDetails => _productDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['productName'] = _productName;
    map['productNameAr'] = _productNameAr;
    map['imagePath'] = _imagePath;
    map['isOffer'] = _isOffer;
    map['isActive'] = _isActive;
    map['deliverectMenuId'] = _deliverectMenuId;
    map['shopId'] = _shopId;
    if (_productDetails != null) {
      map['productDetails'] = _productDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 190
/// title : "Big Mac"
/// titleAr : "بيج ماك لحم"
/// price : 18
/// lastPrice : 18
/// note : "0"
/// noteAr : "0"
/// healthInformation : "0"
/// healthInformationAr : "0"
/// coverImage : "http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/mac6.png"
/// isActive : true
/// availableFromHour : "00:01"
/// availableToHour : "23:59"
/// snoozeEnd : "1970-01-01T00:00:00.000Z"
/// snoozeStart : "1970-01-01T00:00:00.000Z"
/// updateAt : "2023-07-24T08:09:39.2534297"
/// shopId : 36

class ProductDetails {
  ProductDetails({
      int? id, 
      String? title, 
      String? titleAr, 
      double? price,
      double? lastPrice,
      String? note, 
      String? noteAr, 
      String? healthInformation, 
      String? healthInformationAr, 
      String? coverImage, 
      bool? isActive, 
      String? availableFromHour, 
      String? availableToHour, 
      String? snoozeEnd, 
      String? snoozeStart, 
      String? updateAt, 
      int? shopId,}){
    _id = id;
    _title = title;
    _titleAr = titleAr;
    _price = price;
    _lastPrice = lastPrice;
    _note = note;
    _noteAr = noteAr;
    _healthInformation = healthInformation;
    _healthInformationAr = healthInformationAr;
    _coverImage = coverImage;
    _isActive = isActive;
    _availableFromHour = availableFromHour;
    _availableToHour = availableToHour;
    _snoozeEnd = snoozeEnd;
    _snoozeStart = snoozeStart;
    _updateAt = updateAt;
    _shopId = shopId;
}

  ProductDetails.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _titleAr = json['titleAr'];
    _price = json['price'];
    _lastPrice = json['lastPrice'];
    _note = json['note'];
    _noteAr = json['noteAr'];
    _healthInformation = json['healthInformation'];
    _healthInformationAr = json['healthInformationAr'];
    _coverImage = json['coverImage'];
    _isActive = json['isActive'];
    _availableFromHour = json['availableFromHour'];
    _availableToHour = json['availableToHour'];
    _snoozeEnd = json['snoozeEnd'];
    _snoozeStart = json['snoozeStart'];
    _updateAt = json['updateAt'];
    _shopId = json['shopId'];
  }
  int? _id;
  String? _title;
  String? _titleAr;
  double? _price;
  double? _lastPrice;
  String? _note;
  String? _noteAr;
  String? _healthInformation;
  String? _healthInformationAr;
  String? _coverImage;
  bool? _isActive;
  String? _availableFromHour;
  String? _availableToHour;
  String? _snoozeEnd;
  String? _snoozeStart;
  String? _updateAt;
  int? _shopId;

  int? get id => _id;
  String? get title => _title;
  String? get titleAr => _titleAr;
  double? get price => _price;
  double? get lastPrice => _lastPrice;
  String? get note => _note;
  String? get noteAr => _noteAr;
  String? get healthInformation => _healthInformation;
  String? get healthInformationAr => _healthInformationAr;
  String? get coverImage => _coverImage;
  bool? get isActive => _isActive;
  String? get availableFromHour => _availableFromHour;
  String? get availableToHour => _availableToHour;
  String? get snoozeEnd => _snoozeEnd;
  String? get snoozeStart => _snoozeStart;
  String? get updateAt => _updateAt;
  int? get shopId => _shopId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['titleAr'] = _titleAr;
    map['price'] = _price;
    map['lastPrice'] = _lastPrice;
    map['note'] = _note;
    map['noteAr'] = _noteAr;
    map['healthInformation'] = _healthInformation;
    map['healthInformationAr'] = _healthInformationAr;
    map['coverImage'] = _coverImage;
    map['isActive'] = _isActive;
    map['availableFromHour'] = _availableFromHour;
    map['availableToHour'] = _availableToHour;
    map['snoozeEnd'] = _snoozeEnd;
    map['snoozeStart'] = _snoozeStart;
    map['updateAt'] = _updateAt;
    map['shopId'] = _shopId;
    return map;
  }

}