
class ShopSearch {
  ShopSearch({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      ReturnData? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  ShopSearch.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    _returnData = json['returnData'] != null ? ReturnData.fromJson(json['returnData']) : null;
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  ReturnData? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  ReturnData? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.toJson();
    }
    return map;
  }

}


class ReturnData {
  ReturnData({
      int? pageNumber, 
      List<Data>? data, 
      int? pageSize, 
      int? totalItemCount, 
      int? totalPagesCount,}){
    _pageNumber = pageNumber;
    _data = data;
    _pageSize = pageSize;
    _totalItemCount = totalItemCount;
    _totalPagesCount = totalPagesCount;
}

  ReturnData.fromJson(dynamic json) {
    _pageNumber = json['pageNumber'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
    _pageSize = json['pageSize'];
    _totalItemCount = json['totalItemCount'];
    _totalPagesCount = json['totalPagesCount'];
  }
  int? _pageNumber;
  List<Data>? _data;
  int? _pageSize;
  int? _totalItemCount;
  int? _totalPagesCount;

  int? get pageNumber => _pageNumber;
  List<Data>? get data => _data;
  int? get pageSize => _pageSize;
  int? get totalItemCount => _totalItemCount;
  int? get totalPagesCount => _totalPagesCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageNumber'] = _pageNumber;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['pageSize'] = _pageSize;
    map['totalItemCount'] = _totalItemCount;
    map['totalPagesCount'] = _totalPagesCount;
    return map;
  }

}

class Data {
  Data({
      int? id, 
      int? shopBrnachId, 
      String? shopName, 
      String? shopNameAr, 
      String? shopDescription, 
      String? shopDescriptionAr, 
      String? type, 
      String? typeAr, 
      double? distance,
      double? shopeRate,
      bool? isFavorite, 
      bool? isOpen, 
      String? avatar, 
      String? headerImage, 
      String? descriptionLocation, 
      String? descriptionLocationAr,
      double? latitude,
      double? longitude,
      String? firstShiftAvailableTimeFrom, 
      String? firstShiftAvailableTimeTo, 
      dynamic secondShiftAvailableTimeFrom, 
      dynamic secondShiftAvailableTimeTo, 
      bool? isBusy, 
      int? dispatchType,}){
    _id = id;
    _shopBrnachId = shopBrnachId;
    _shopName = shopName;
    _shopNameAr = shopNameAr;
    _shopDescription = shopDescription;
    _shopDescriptionAr = shopDescriptionAr;
    _type = type;
    _typeAr = typeAr;
    _distance = distance;
    _shopeRate = shopeRate;
    _isFavorite = isFavorite;
    _isOpen = isOpen;
    _avatar = avatar;
    _headerImage = headerImage;
    _descriptionLocation = descriptionLocation;
    _descriptionLocationAr = descriptionLocationAr;
    _latitude = latitude;
    _longitude = longitude;
    _firstShiftAvailableTimeFrom = firstShiftAvailableTimeFrom;
    _firstShiftAvailableTimeTo = firstShiftAvailableTimeTo;
    _secondShiftAvailableTimeFrom = secondShiftAvailableTimeFrom;
    _secondShiftAvailableTimeTo = secondShiftAvailableTimeTo;
    _isBusy = isBusy;
    _dispatchType = dispatchType;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _shopBrnachId = json['shopBrnachId'];
    _shopName = json['shopName'];
    _shopNameAr = json['shopNameAr'];
    _shopDescription = json['shopDescription'];
    _shopDescriptionAr = json['shopDescriptionAr'];
    _type = json['type'];
    _typeAr = json['typeAr'];
    _distance = json['distance'];
    _shopeRate = json['shopeRate'];
    _isFavorite = json['isFavorite'];
    _isOpen = json['isOpen'];
    _avatar = json['avatar'];
    _headerImage = json['headerImage'];
    _descriptionLocation = json['descriptionLocation'];
    _descriptionLocationAr = json['descriptionLocationAr'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _firstShiftAvailableTimeFrom = json['firstShiftAvailableTimeFrom'];
    _firstShiftAvailableTimeTo = json['firstShiftAvailableTimeTo'];
    _secondShiftAvailableTimeFrom = json['secondShiftAvailableTimeFrom'];
    _secondShiftAvailableTimeTo = json['secondShiftAvailableTimeTo'];
    _isBusy = json['isBusy'];
    _dispatchType = json['dispatchType'];
  }
  int? _id;
  int? _shopBrnachId;
  String? _shopName;
  String? _shopNameAr;
  String? _shopDescription;
  String? _shopDescriptionAr;
  String? _type;
  String? _typeAr;
  double? _distance;
  double? _shopeRate;
  bool? _isFavorite;
  bool? _isOpen;
  String? _avatar;
  String? _headerImage;
  String? _descriptionLocation;
  String? _descriptionLocationAr;
  double? _latitude;
  double? _longitude;
  String? _firstShiftAvailableTimeFrom;
  String? _firstShiftAvailableTimeTo;
  dynamic _secondShiftAvailableTimeFrom;
  dynamic _secondShiftAvailableTimeTo;
  bool? _isBusy;
  int? _dispatchType;

  int? get id => _id;
  int? get shopBrnachId => _shopBrnachId;
  String? get shopName => _shopName;
  String? get shopNameAr => _shopNameAr;
  String? get shopDescription => _shopDescription;
  String? get shopDescriptionAr => _shopDescriptionAr;
  String? get type => _type;
  String? get typeAr => _typeAr;
  double? get distance => _distance;
  double? get shopeRate => _shopeRate;
  bool? get isFavorite => _isFavorite;
  bool? get isOpen => _isOpen;
  String? get avatar => _avatar;
  String? get headerImage => _headerImage;
  String? get descriptionLocation => _descriptionLocation;
  String? get descriptionLocationAr => _descriptionLocationAr;
  double? get latitude => _latitude;
  double? get longitude => _longitude;
  String? get firstShiftAvailableTimeFrom => _firstShiftAvailableTimeFrom;
  String? get firstShiftAvailableTimeTo => _firstShiftAvailableTimeTo;
  dynamic get secondShiftAvailableTimeFrom => _secondShiftAvailableTimeFrom;
  dynamic get secondShiftAvailableTimeTo => _secondShiftAvailableTimeTo;
  bool? get isBusy => _isBusy;
  int? get dispatchType => _dispatchType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['shopBrnachId'] = _shopBrnachId;
    map['shopName'] = _shopName;
    map['shopNameAr'] = _shopNameAr;
    map['shopDescription'] = _shopDescription;
    map['shopDescriptionAr'] = _shopDescriptionAr;
    map['type'] = _type;
    map['typeAr'] = _typeAr;
    map['distance'] = _distance;
    map['shopeRate'] = _shopeRate;
    map['isFavorite'] = _isFavorite;
    map['isOpen'] = _isOpen;
    map['avatar'] = _avatar;
    map['headerImage'] = _headerImage;
    map['descriptionLocation'] = _descriptionLocation;
    map['descriptionLocationAr'] = _descriptionLocationAr;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['firstShiftAvailableTimeFrom'] = _firstShiftAvailableTimeFrom;
    map['firstShiftAvailableTimeTo'] = _firstShiftAvailableTimeTo;
    map['secondShiftAvailableTimeFrom'] = _secondShiftAvailableTimeFrom;
    map['secondShiftAvailableTimeTo'] = _secondShiftAvailableTimeTo;
    map['isBusy'] = _isBusy;
    map['dispatchType'] = _dispatchType;
    return map;
  }

}