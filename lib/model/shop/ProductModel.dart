class ProductModel {
  ProductModel({
    bool? isSucceeded,
    int? apiStatusCode,
    String? errorMessage,
    ReturnData? returnData,
  }) {
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
  }

  ProductModel.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    _returnData = json['returnData'] != null
        ? ReturnData.fromJson(json['returnData'])
        : null;
  }

  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  ReturnData? _returnData;

  bool? get isSucceeded => _isSucceeded;

  int? get apiStatusCode => _apiStatusCode;

  String? get errorMessage => _errorMessage;

  ReturnData? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.toJson();
    }
    return map;
  }
}

class ReturnData {
  ReturnData({
    int? id,
    String? title,
    String? titleAr,
    double? price,
    double? lastPrice,
    String? note,
    String? noteAr,
    int? productId,
    String? coverImage,
    String? healthInformation,
    String? healthInformationAr,
    List<ProductComponants>? productComponants,
    List<ProductAdditionalOptions>? productAdditionalOptions,
    List<ProductDetailsSizes>? productDetailsSizes,
    List<AdditionalComponentTitles>? additionalComponentTitles,
  }) {
    _id = id;
    _title = title;
    _titleAr = titleAr;
    _price = price;
    _lastPrice = lastPrice;
    _note = note;
    _noteAr = noteAr;
    _productId = productId;
    _coverImage = coverImage;
    _healthInformation = healthInformation;
    _healthInformationAr = healthInformationAr;
    _productComponants = productComponants;
    _productAdditionalOptions = productAdditionalOptions;
    _productDetailsSizes = productDetailsSizes;
    _additionalComponentTitles = additionalComponentTitles;
  }

  ReturnData.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _titleAr = json['titleAr'];
    _price = json['price'];
    _lastPrice = json['lastPrice'];
    _note = json['note'];
    _noteAr = json['noteAr'];
    _productId = json['productId'];
    _coverImage = json['coverImage'];
    _healthInformation = json['healthInformation'];
    _healthInformationAr = json['healthInformationAr'];
    if (json['productComponants'] != null) {
      _productComponants = [];
      json['productComponants'].forEach((v) {
        _productComponants?.add(ProductComponants.fromJson(v));
      });
    }
    if (json['productAdditionalOptions'] != null) {
      _productAdditionalOptions = [];
      json['productAdditionalOptions'].forEach((v) {
        _productAdditionalOptions?.add(ProductAdditionalOptions.fromJson(v));
      });
    }
    if (json['productDetailsSizes'] != null) {
      _productDetailsSizes = [];
      json['productDetailsSizes'].forEach((v) {
        _productDetailsSizes?.add(ProductDetailsSizes.fromJson(v));
      });
    }
    if (json['additionalComponentTitles'] != null) {
      _additionalComponentTitles = [];
      json['additionalComponentTitles'].forEach((v) {
        _additionalComponentTitles?.add(AdditionalComponentTitles.fromJson(v));
      });
    }
  }

  int? _id;
  String? _title;
  String? _titleAr;
  double? _price;
  double? _lastPrice;
  String? _note;
  String? _noteAr;
  int? _productId;
  String? _coverImage;
  String? _healthInformation;
  String? _healthInformationAr;
  List<ProductComponants>? _productComponants;
  List<ProductAdditionalOptions>? _productAdditionalOptions;
  List<ProductDetailsSizes>? _productDetailsSizes;
  List<AdditionalComponentTitles>? _additionalComponentTitles;

  int? get id => _id;

  String? get title => _title;

  String? get titleAr => _titleAr;

  double? get price => _price;

  double? get lastPrice => _lastPrice;

  String? get note => _note;

  String? get noteAr => _noteAr;

  int? get productId => _productId;

  String? get coverImage => _coverImage;

  String? get healthInformation => _healthInformation;

  String? get healthInformationAr => _healthInformationAr;

  List<ProductComponants>? get productComponants => _productComponants;

  List<ProductAdditionalOptions>? get productAdditionalOptions =>
      _productAdditionalOptions;

  List<ProductDetailsSizes>? get productDetailsSizes => _productDetailsSizes;

  List<AdditionalComponentTitles>? get additionalComponentTitles =>
      _additionalComponentTitles;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['titleAr'] = _titleAr;
    map['price'] = _price;
    map['lastPrice'] = _lastPrice;
    map['note'] = _note;
    map['noteAr'] = _noteAr;
    map['productId'] = _productId;
    map['coverImage'] = _coverImage;
    map['healthInformation'] = _healthInformation;
    map['healthInformationAr'] = _healthInformationAr;
    if (_productComponants != null) {
      map['productComponants'] =
          _productComponants?.map((v) => v.toJson()).toList();
    }
    if (_productAdditionalOptions != null) {
      map['productAdditionalOptions'] =
          _productAdditionalOptions?.map((v) => v.toJson()).toList();
    }
    if (_productDetailsSizes != null) {
      map['productDetailsSizes'] =
          _productDetailsSizes?.map((v) => v.toJson()).toList();
    }
    if (_additionalComponentTitles != null) {
      map['additionalComponentTitles'] =
          _additionalComponentTitles?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class AdditionalComponentTitles {
  AdditionalComponentTitles({
    int? id,
    String? createAt,
    String? updateAt,
    String? title,
    String? titleAr,
    bool? isActive,
    int? numberOfSelect,
    int? minOfSelect,
    bool? isRequired,
    int? productDetailsId,
    String? deliverectModifierGroupId,
    String? deliverectPLU,
    List<AdditionalComponents>? additionalComponents,
  }) {
    _id = id;
    _createAt = createAt;
    _updateAt = updateAt;
    _title = title;
    _titleAr = titleAr;
    _isActive = isActive;
    _numberOfSelect = numberOfSelect;
    _minOfSelect = minOfSelect;
    _isRequired = isRequired;
    _productDetailsId = productDetailsId;
    _deliverectModifierGroupId = deliverectModifierGroupId;
    _deliverectPLU = deliverectPLU;
    _additionalComponents = additionalComponents;
  }

  AdditionalComponentTitles.fromJson(dynamic json) {
    _id = json['id'];
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
    _title = json['title'];
    _titleAr = json['titleAr'];
    _isActive = json['isActive'];
    _numberOfSelect = json['numberOfSelect'];
    _minOfSelect = json['minOfSelect'];
    _isRequired = json['isRequired'];
    _productDetailsId = json['productDetailsId'];
    _deliverectModifierGroupId = json['deliverectModifierGroupId'];
    _deliverectPLU = json['deliverectPLU'];
    if (json['additionalComponents'] != null) {
      _additionalComponents = [];
      json['additionalComponents'].forEach((v) {
        _additionalComponents?.add(AdditionalComponents.fromJson(v));
      });
    }
  }

  int? _id;
  String? _createAt;
  String? _updateAt;
  String? _title;
  String? _titleAr;
  bool? _isActive;
  int? _numberOfSelect;
  int? _minOfSelect;
  bool? _isRequired;
  int? _productDetailsId;
  String? _deliverectModifierGroupId;
  String? _deliverectPLU;
  List<AdditionalComponents>? _additionalComponents;

  int? get id => _id;

  String? get createAt => _createAt;

  String? get updateAt => _updateAt;

  String? get title => _title;

  String? get titleAr => _titleAr;

  bool? get isActive => _isActive;

  int? get numberOfSelect => _numberOfSelect;

  int? get minOfSelect => _minOfSelect;

  bool? get isRequired => _isRequired;

  int? get productDetailsId => _productDetailsId;

  String? get deliverectModifierGroupId => _deliverectModifierGroupId;

  String? get deliverectPLU => _deliverectPLU;

  List<AdditionalComponents>? get additionalComponents => _additionalComponents;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    map['title'] = _title;
    map['titleAr'] = _titleAr;
    map['isActive'] = _isActive;
    map['numberOfSelect'] = _numberOfSelect;
    map['minOfSelect'] = _minOfSelect;
    map['isRequired'] = _isRequired;
    map['productDetailsId'] = _productDetailsId;
    map['deliverectModifierGroupId'] = _deliverectModifierGroupId;
    map['deliverectPLU'] = _deliverectPLU;
    if (_additionalComponents != null) {
      map['additionalComponents'] =
          _additionalComponents?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class AdditionalComponents {
  AdditionalComponents({
    int? id,
    String? createAt,
    String? updateAt,
    String? componentName,
    String? componentNameAr,
    double? price,
    int? additionalComponentTitleId,
    String? deliverectModifierId,
    String? deliverectPLU,
    String? snoozeStart,
    String? snoozeEnd,
    int? minOfSelect,
    int? maxOfSelect,
    bool? defaultQuantity,
    List<SubAdditionalComponentTitles>? subAdditionalComponentTitles,
  }) {
    _id = id;
    _createAt = createAt;
    _updateAt = updateAt;
    _componentName = componentName;
    _componentNameAr = componentNameAr;
    _price = price;
    _additionalComponentTitleId = additionalComponentTitleId;
    _deliverectModifierId = deliverectModifierId;
    _deliverectPLU = deliverectPLU;
    _snoozeStart = snoozeStart;
    _snoozeEnd = snoozeEnd;
    _minOfSelect = minOfSelect;
    _maxOfSelect = maxOfSelect;
    _defaultQuantity = defaultQuantity;
    _subAdditionalComponentTitles = subAdditionalComponentTitles;
  }

  AdditionalComponents.fromJson(dynamic json) {
    _id = json['id'];
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
    _componentName = json['componentName'];
    _componentNameAr = json['componentNameAr'];
    _price = json['price'];
    _additionalComponentTitleId = json['additionalComponentTitleId'];
    _deliverectModifierId = json['deliverectModifierId'];
    _deliverectPLU = json['deliverectPLU'];
    _snoozeStart = json['snoozeStart'];
    _snoozeEnd = json['snoozeEnd'];
    _minOfSelect = json['minOfSelect'];
    _maxOfSelect = json['maxOfSelect'];
    _defaultQuantity = json['defaultQuantity'];
    if (json['subAdditionalComponentTitles'] != null) {
      _subAdditionalComponentTitles = [];
      json['subAdditionalComponentTitles'].forEach((v) {
        _subAdditionalComponentTitles
            ?.add(SubAdditionalComponentTitles.fromJson(v));
      });
    }
  }

  int? _id;
  String? _createAt;
  String? _updateAt;
  String? _componentName;
  String? _componentNameAr;
  double? _price;
  int? _additionalComponentTitleId;
  String? _deliverectModifierId;
  String? _deliverectPLU;
  String? _snoozeStart;
  String? _snoozeEnd;
  int? _minOfSelect;
  int? _maxOfSelect;
  bool? _defaultQuantity;
  List<SubAdditionalComponentTitles>? _subAdditionalComponentTitles;

  int? get id => _id;

  String? get createAt => _createAt;

  String? get updateAt => _updateAt;

  String? get componentName => _componentName;

  String? get componentNameAr => _componentNameAr;

  double? get price => _price;

  int? get additionalComponentTitleId => _additionalComponentTitleId;

  String? get deliverectModifierId => _deliverectModifierId;

  String? get deliverectPLU => _deliverectPLU;

  String? get snoozeStart => _snoozeStart;

  String? get snoozeEnd => _snoozeEnd;

  int? get minOfSelect => _minOfSelect;

  int? get maxOfSelect => _maxOfSelect;

  bool? get defaultQuantity => _defaultQuantity;

  List<SubAdditionalComponentTitles>? get subAdditionalComponentTitles =>
      _subAdditionalComponentTitles;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    map['componentName'] = _componentName;
    map['componentNameAr'] = _componentNameAr;
    map['price'] = _price;
    map['additionalComponentTitleId'] = _additionalComponentTitleId;
    map['deliverectModifierId'] = _deliverectModifierId;
    map['deliverectPLU'] = _deliverectPLU;
    map['snoozeStart'] = _snoozeStart;
    map['snoozeEnd'] = _snoozeEnd;
    map['minOfSelect'] = _minOfSelect;
    map['maxOfSelect'] = _maxOfSelect;
    map['defaultQuantity'] = _defaultQuantity;
    if (_subAdditionalComponentTitles != null) {
      map['subAdditionalComponentTitles'] =
          _subAdditionalComponentTitles?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class SubAdditionalComponentTitles {
  SubAdditionalComponentTitles({
    int? id,
    String? title,
    String? titleAr,
    bool? isActive,
    int? minOfSelect,
    int? numberOfSelect,
    bool? isRequired,
    String? deliverectPLU,
    String? deliverectModifierGroupId,
    int? additionalComponentId,
    List<SubAdditionalComponents>? subAdditionalComponents,
    String? createAt,
    String? updateAt,
  }) {
    _id = id;
    _title = title;
    _titleAr = titleAr;
    _isActive = isActive;
    _minOfSelect = minOfSelect;
    _numberOfSelect = numberOfSelect;
    _isRequired = isRequired;
    _deliverectPLU = deliverectPLU;
    _deliverectModifierGroupId = deliverectModifierGroupId;
    _additionalComponentId = additionalComponentId;
    _subAdditionalComponents = subAdditionalComponents;
    _createAt = createAt;
    _updateAt = updateAt;
  }

  SubAdditionalComponentTitles.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _titleAr = json['titleAr'];
    _isActive = json['isActive'];
    _minOfSelect = json['minOfSelect'];
    _numberOfSelect = json['numberOfSelect'];
    _isRequired = json['isRequired'];
    _deliverectPLU = json['deliverectPLU'];
    _deliverectModifierGroupId = json['deliverectModifierGroupId'];
    _additionalComponentId = json['additionalComponentId'];
    if (json['subAdditionalComponents'] != null) {
      _subAdditionalComponents = [];
      json['subAdditionalComponents'].forEach((v) {
        _subAdditionalComponents?.add(SubAdditionalComponents.fromJson(v));
      });
    }
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
  }

  int? _id;
  String? _title;
  String? _titleAr;
  bool? _isActive;
  int? _minOfSelect;
  int? _numberOfSelect;
  bool? _isRequired;
  String? _deliverectPLU;
  String? _deliverectModifierGroupId;
  int? _additionalComponentId;
  List<SubAdditionalComponents>? _subAdditionalComponents;
  String? _createAt;
  String? _updateAt;

  int? get id => _id;

  String? get title => _title;

  String? get titleAr => _titleAr;

  bool? get isActive => _isActive;

  int? get minOfSelect => _minOfSelect;

  int? get numberOfSelect => _numberOfSelect;

  bool? get isRequired => _isRequired;

  String? get deliverectPLU => _deliverectPLU;

  String? get deliverectModifierGroupId => _deliverectModifierGroupId;

  int? get additionalComponentId => _additionalComponentId;

  List<SubAdditionalComponents>? get subAdditionalComponents =>
      _subAdditionalComponents;

  String? get createAt => _createAt;

  String? get updateAt => _updateAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['titleAr'] = _titleAr;
    map['isActive'] = _isActive;
    map['minOfSelect'] = _minOfSelect;
    map['numberOfSelect'] = _numberOfSelect;
    map['isRequired'] = _isRequired;
    map['deliverectPLU'] = _deliverectPLU;
    map['deliverectModifierGroupId'] = _deliverectModifierGroupId;
    map['additionalComponentId'] = _additionalComponentId;
    if (_subAdditionalComponents != null) {
      map['subAdditionalComponents'] =
          _subAdditionalComponents?.map((v) => v.toJson()).toList();
    }
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    return map;
  }
}

class SubAdditionalComponents {
  SubAdditionalComponents({
    int? id,
    String? componentName,
    String? componentNameAr,
    double? price,
    String? deliverectPLU,
    String? deliverectModifierId,
    String? snoozeStart,
    String? snoozeEnd,
    int? subAdditionalComponentTitleId,
    String? createAt,
    String? updateAt,
    int? minOfSelect,
    int? maxOfSelect,
    bool? defaultQuantity,
  }) {
    _id = id;
    _componentName = componentName;
    _componentNameAr = componentNameAr;
    _price = price;
    _deliverectPLU = deliverectPLU;
    _deliverectModifierId = deliverectModifierId;
    _snoozeStart = snoozeStart;
    _snoozeEnd = snoozeEnd;
    _subAdditionalComponentTitleId = subAdditionalComponentTitleId;
    _createAt = createAt;
    _updateAt = updateAt;
    _minOfSelect = minOfSelect;
    _maxOfSelect = maxOfSelect;
    _defaultQuantity = defaultQuantity;
  }

  SubAdditionalComponents.fromJson(dynamic json) {
    _id = json['id'];
    _componentName = json['componentName'];
    _componentNameAr = json['componentNameAr'];
    _price = json['price'];
    _deliverectPLU = json['deliverectPLU'];
    _deliverectModifierId = json['deliverectModifierId'];
    _snoozeStart = json['snoozeStart'];
    _snoozeEnd = json['snoozeEnd'];
    _subAdditionalComponentTitleId = json['subAdditionalComponentTitleId'];
    _createAt = json['createAt'];
    _updateAt = json['updateAt'];
    _minOfSelect = json['minOfSelect'];
    _maxOfSelect = json['maxOfSelect'];
    _defaultQuantity = json['defaultQuantity'];
  }

  int? _id;
  String? _componentName;
  String? _componentNameAr;
  double? _price;
  String? _deliverectPLU;
  String? _deliverectModifierId;
  String? _snoozeStart;
  String? _snoozeEnd;
  int? _subAdditionalComponentTitleId;
  String? _createAt;
  String? _updateAt;
  int? _minOfSelect;
  int? _maxOfSelect;
  bool? _defaultQuantity;

  int? get id => _id;

  String? get componentName => _componentName;

  String? get componentNameAr => _componentNameAr;

  double? get price => _price;

  String? get deliverectPLU => _deliverectPLU;

  String? get deliverectModifierId => _deliverectModifierId;

  String? get snoozeStart => _snoozeStart;

  String? get snoozeEnd => _snoozeEnd;

  int? get subAdditionalComponentTitleId => _subAdditionalComponentTitleId;

  String? get createAt => _createAt;

  String? get updateAt => _updateAt;

  int? get minOfSelect => _minOfSelect;

  int? get maxOfSelect => _maxOfSelect;

  bool? get defaultQuantity => _defaultQuantity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['componentName'] = _componentName;
    map['componentNameAr'] = _componentNameAr;
    map['price'] = _price;
    map['deliverectPLU'] = _deliverectPLU;
    map['deliverectModifierId'] = _deliverectModifierId;
    map['snoozeStart'] = _snoozeStart;
    map['snoozeEnd'] = _snoozeEnd;
    map['subAdditionalComponentTitleId'] = _subAdditionalComponentTitleId;
    map['createAt'] = _createAt;
    map['updateAt'] = _updateAt;
    map['minOfSelect'] = _minOfSelect;
    map['maxOfSelect'] = _maxOfSelect;
    map['defaultQuantity'] = _defaultQuantity;
    return map;
  }
}

class ProductDetailsSizes {
  ProductDetailsSizes({
    int? id,
    double? price,
    double? lastPrice,
    String? itemSizeNameEn,
    String? itemSizeNameAr,
    int? itemSizeId,
    bool? isActive,
    String? deliverectPLU,
  }) {
    _id = id;
    _price = price;
    _lastPrice = lastPrice;
    _itemSizeNameEn = itemSizeNameEn;
    _itemSizeNameAr = itemSizeNameAr;
    _itemSizeId = itemSizeId;
    _isActive = isActive;
    _deliverectPLU = deliverectPLU;
  }

  ProductDetailsSizes.fromJson(dynamic json) {
    _id = json['id'];
    _price = json['price'];
    _lastPrice = json['lastPrice'];
    _itemSizeNameEn = json['itemSizeNameEn'];
    _itemSizeNameAr = json['itemSizeNameAr'];
    _itemSizeId = json['itemSizeId'];
    _isActive = json['isActive'];
    _deliverectPLU = json['deliverectPLU'];
  }

  int? _id;
  double? _price;
  double? _lastPrice;
  String? _itemSizeNameEn;
  String? _itemSizeNameAr;
  int? _itemSizeId;
  bool? _isActive;
  String? _deliverectPLU;

  int? get id => _id;

  double? get price => _price;

  double? get lastPrice => _lastPrice;

  String? get itemSizeNameEn => _itemSizeNameEn;

  String? get itemSizeNameAr => _itemSizeNameAr;

  int? get itemSizeId => _itemSizeId;

  bool? get isActive => _isActive;

  String? get deliverectPLU => _deliverectPLU;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['price'] = _price;
    map['lastPrice'] = _lastPrice;
    map['itemSizeNameEn'] = _itemSizeNameEn;
    map['itemSizeNameAr'] = _itemSizeNameAr;
    map['itemSizeId'] = _itemSizeId;
    map['isActive'] = _isActive;
    map['deliverectPLU'] = _deliverectPLU;
    return map;
  }
}

class ProductAdditionalOptions {
  ProductAdditionalOptions({
    int? id,
    String? name,
    String? nameAr,
    double? price,
  }) {
    _id = id;
    _name = name;
    _nameAr = nameAr;
    _price = price;
  }

  ProductAdditionalOptions.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _nameAr = json['nameAr'];
    _price = json['price'];
  }

  int? _id;
  String? _name;
  String? _nameAr;
  double? _price;

  int? get id => _id;

  String? get name => _name;

  String? get nameAr => _nameAr;

  double? get price => _price;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['nameAr'] = _nameAr;
    map['price'] = _price;
    return map;
  }
}

class ProductComponants {
  ProductComponants({
    int? id,
    String? componantName,
    String? componantNameAr,
    bool? isBasicComponent,
    int? productDetailsId,
    String? createAt,
    bool? isActive,
  }) {
    _id = id;
    _componantName = componantName;
    _componantNameAr = componantNameAr;
    _isBasicComponent = isBasicComponent;
    _productDetailsId = productDetailsId;
    _createAt = createAt;
    _isActive = isActive;
  }

  ProductComponants.fromJson(dynamic json) {
    _id = json['id'];
    _componantName = json['componantName'];
    _componantNameAr = json['componantNameAr'];
    _isBasicComponent = json['isBasicComponent'];
    _productDetailsId = json['productDetailsId'];
    _createAt = json['createAt'];
    _isActive = json['isActive'];
  }

  int? _id;
  String? _componantName;
  String? _componantNameAr;
  bool? _isBasicComponent;
  int? _productDetailsId;
  String? _createAt;
  bool? _isActive;

  int? get id => _id;

  String? get componantName => _componantName;

  String? get componantNameAr => _componantNameAr;

  bool? get isBasicComponent => _isBasicComponent;

  int? get productDetailsId => _productDetailsId;

  String? get createAt => _createAt;

  bool? get isActive => _isActive;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['componantName'] = _componantName;
    map['componantNameAr'] = _componantNameAr;
    map['isBasicComponent'] = _isBasicComponent;
    map['productDetailsId'] = _productDetailsId;
    map['createAt'] = _createAt;
    map['isActive'] = _isActive;
    return map;
  }
}
