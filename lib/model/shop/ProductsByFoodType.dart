import 'dart:ffi';

class ProductsByFoodType {
  ProductsByFoodType({
    bool? isSucceeded,
    int? apiStatusCode,
    String? errorMessage,
    List<ReturnData>? returnData,
  }) {
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
  }

  ProductsByFoodType.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    if (json['returnData'] != null) {
      _returnData = [];
      json['returnData'].forEach((v) {
        _returnData?.add(ReturnData.fromJson(v));
      });
    }
  }

  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  List<ReturnData>? _returnData;

  bool? get isSucceeded => _isSucceeded;

  int? get apiStatusCode => _apiStatusCode;

  String? get errorMessage => _errorMessage;

  List<ReturnData>? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ReturnData {
  ReturnData({
    int? shopId,
    String? shopName,
    String? shopNameAr,
    String? shopAvatar,
    String? shopHeaderImage,
    String? shopBrnachDescriptionLocation,
    String? shopBrnachDescriptionLocationAr,
    double? distance,
    double? shopBrnachId,
    List<ProductDetails>? productDetails,
  }) {
    _shopId = shopId;
    _shopName = shopName;
    _shopNameAr = shopNameAr;
    _shopAvatar = shopAvatar;
    _shopHeaderImage = shopHeaderImage;
    _shopBrnachDescriptionLocation = shopBrnachDescriptionLocation;
    _shopBrnachDescriptionLocationAr = shopBrnachDescriptionLocationAr;
    _distance = distance;
    _shopBrnachId = shopBrnachId;
    _productDetails = productDetails;
  }

  ReturnData.fromJson(dynamic json) {
    _shopId = json['shopId'];
    _shopName = json['shopName'];
    _shopNameAr = json['shopNameAr'];
    _shopAvatar = json['shopAvatar'];
    _shopHeaderImage = json['shopHeaderImage'];
    _shopBrnachDescriptionLocation = json['shopBrnachDescriptionLocation'];
    _shopBrnachDescriptionLocationAr = json['shopBrnachDescriptionLocationAr'];
    _distance = json['distance'];
    _shopBrnachId = json['shopBrnachId'];
    if (json['productDetails'] != null) {
      _productDetails = [];
      json['productDetails'].forEach((v) {
        _productDetails?.add(ProductDetails.fromJson(v));
      });
    }
  }

  int? _shopId;
  String? _shopName;
  String? _shopNameAr;
  String? _shopAvatar;
  String? _shopHeaderImage;
  String? _shopBrnachDescriptionLocation;
  String? _shopBrnachDescriptionLocationAr;
  double? _distance;
  double? _shopBrnachId;
  List<ProductDetails>? _productDetails;

  int? get shopId => _shopId;

  String? get shopName => _shopName;

  String? get shopNameAr => _shopNameAr;

  String? get shopAvatar => _shopAvatar;

  String? get shopHeaderImage => _shopHeaderImage;

  String? get shopBrnachDescriptionLocation => _shopBrnachDescriptionLocation;

  String? get shopBrnachDescriptionLocationAr =>
      _shopBrnachDescriptionLocationAr;

  double? get distance => _distance;

  double? get shopBrnachId => _shopBrnachId;

  List<ProductDetails>? get productDetails => _productDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shopId'] = _shopId;
    map['shopName'] = _shopName;
    map['shopNameAr'] = _shopNameAr;
    map['shopAvatar'] = _shopAvatar;
    map['shopHeaderImage'] = _shopHeaderImage;
    map['shopBrnachDescriptionLocation'] = _shopBrnachDescriptionLocation;
    map['shopBrnachDescriptionLocationAr'] = _shopBrnachDescriptionLocationAr;
    map['distance'] = _distance;
    map['shopBrnachId'] = _shopBrnachId;
    if (_productDetails != null) {
      map['productDetails'] = _productDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ProductDetails {
  ProductDetails({
    int? id,
    String? title,
    String? titleAr,
    double? price,
    double? lastPrice,
    String? note,
    String? noteAr,
    String? healthInformation,
    String? healthInformationAr,
    String? coverImage,
    bool? isActive,
    String? availableFromHour,
    String? availableToHour,
    dynamic snoozeEnd,
    dynamic snoozeStart,
    String? updateAt,
    int? shopId,
  }) {
    _id = id;
    _title = title;
    _titleAr = titleAr;
    _price = price;
    _lastPrice = lastPrice;
    _note = note;
    _noteAr = noteAr;
    _healthInformation = healthInformation;
    _healthInformationAr = healthInformationAr;
    _coverImage = coverImage;
    _isActive = isActive;
    _availableFromHour = availableFromHour;
    _availableToHour = availableToHour;
    _snoozeEnd = snoozeEnd;
    _snoozeStart = snoozeStart;
    _updateAt = updateAt;
    _shopId = shopId;
  }

  ProductDetails.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _titleAr = json['titleAr'];
    _price = json['price'];
    _lastPrice = json['lastPrice'];
    _note = json['note'];
    _noteAr = json['noteAr'];
    _healthInformation = json['healthInformation'];
    _healthInformationAr = json['healthInformationAr'];
    _coverImage = json['coverImage'];
    _isActive = json['isActive'];
    _availableFromHour = json['availableFromHour'];
    _availableToHour = json['availableToHour'];
    _snoozeEnd = json['snoozeEnd'];
    _snoozeStart = json['snoozeStart'];
    _updateAt = json['updateAt'];
    _shopId = json['shopId'];
  }

  int? _id;
  String? _title;
  String? _titleAr;
  double? _price;
  double? _lastPrice;
  String? _note;
  String? _noteAr;
  String? _healthInformation;
  String? _healthInformationAr;
  String? _coverImage;
  bool? _isActive;
  String? _availableFromHour;
  String? _availableToHour;
  dynamic _snoozeEnd;
  dynamic _snoozeStart;
  String? _updateAt;
  int? _shopId;

  int? get id => _id;

  String? get title => _title;

  String? get titleAr => _titleAr;

  double? get price => _price;

  double? get lastPrice => _lastPrice;

  String? get note => _note;

  String? get noteAr => _noteAr;

  String? get healthInformation => _healthInformation;

  String? get healthInformationAr => _healthInformationAr;

  String? get coverImage => _coverImage;

  bool? get isActive => _isActive;

  String? get availableFromHour => _availableFromHour;

  String? get availableToHour => _availableToHour;

  dynamic get snoozeEnd => _snoozeEnd;

  dynamic get snoozeStart => _snoozeStart;

  String? get updateAt => _updateAt;

  int? get shopId => _shopId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['titleAr'] = _titleAr;
    map['price'] = _price;
    map['lastPrice'] = _lastPrice;
    map['note'] = _note;
    map['noteAr'] = _noteAr;
    map['healthInformation'] = _healthInformation;
    map['healthInformationAr'] = _healthInformationAr;
    map['coverImage'] = _coverImage;
    map['isActive'] = _isActive;
    map['availableFromHour'] = _availableFromHour;
    map['availableToHour'] = _availableToHour;
    map['snoozeEnd'] = _snoozeEnd;
    map['snoozeStart'] = _snoozeStart;
    map['updateAt'] = _updateAt;
    map['shopId'] = _shopId;
    return map;
  }
}
