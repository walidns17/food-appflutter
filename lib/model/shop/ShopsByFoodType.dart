/// isSucceeded : true
/// apiStatusCode : 200
/// errorMessage : ""
/// returnData : {"pageNumber":1,"data":[{"id":36,"shopBrnachId":125,"shopName":"McDonalds","shopNameAr":"ماكدونالدز","shopDescription":"Fast Food","shopDescriptionAr":"وجبات سريعة","type":null,"typeAr":null,"distance":0.5387863417484943,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/788fa90e-84a8-45af-aff5-db28ea1550a3/mcdonalds-png-logo-picture-3.png","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/98ecd566-993b-43d1-945b-005d05722721/fasterImage/868.PNG","descriptionLocation":"dammam rd 2","descriptionLocationAr":"طريق الدمام 2","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0},{"id":45,"shopBrnachId":301,"shopName":"AlBaik","shopNameAr":"البيك","shopDescription":"Broasted","shopDescriptionAr":"بروست","type":null,"typeAr":null,"distance":1.3815025196866604,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Logo2_636612266123864538.jpg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/62f1110c-0e7b-4e5d-9876-d1fd2c4ebf79/PIZZA0030.png","descriptionLocation":"yarmok","descriptionLocationAr":"اليرموك","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0},{"id":10418,"shopBrnachId":16672,"shopName":"Burgerina","shopNameAr":"برغرينا","shopDescription":"Fast Food Burger Restaurant","shopDescriptionAr":"مطعم برغر وجبات سريعة","type":null,"typeAr":null,"distance":1.6246798316746685,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/fbf5d84a-1abd-48fc-a346-cc5eca9a7c88/C8694EDF-24FF-4B33-8ED5-03B098E18540.jpeg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/280b6e72-3dca-42f0-a8ad-1dbe1e058b53/Untitled0design0060.png","descriptionLocation":"Qurtoba","descriptionLocationAr":"قرطبة","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":true,"dispatchType":0},{"id":40,"shopBrnachId":10991,"shopName":"Hardees","shopNameAr":"هارديز","shopDescription":"Fast Food","shopDescriptionAr":"وجبات سريعة","type":null,"typeAr":null,"distance":2.3624141271881673,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Hardees_Logo_V_TM_-__636675128508671913.jpg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/6ec20f65-8688-4327-8272-5664c2e76698/Untitled0design00200.png","descriptionLocation":"hard3","descriptionLocationAr":"هارد3","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0}],"pageSize":10,"totalItemCount":4,"totalPagesCount":1}

class ShopsByFoodType {
  ShopsByFoodType({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      ReturnData? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  ShopsByFoodType.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    _returnData = json['returnData'] != null ? ReturnData.fromJson(json['returnData']) : null;
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  ReturnData? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  ReturnData? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.toJson();
    }
    return map;
  }

}

/// pageNumber : 1
/// data : [{"id":36,"shopBrnachId":125,"shopName":"McDonalds","shopNameAr":"ماكدونالدز","shopDescription":"Fast Food","shopDescriptionAr":"وجبات سريعة","type":null,"typeAr":null,"distance":0.5387863417484943,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/788fa90e-84a8-45af-aff5-db28ea1550a3/mcdonalds-png-logo-picture-3.png","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/98ecd566-993b-43d1-945b-005d05722721/fasterImage/868.PNG","descriptionLocation":"dammam rd 2","descriptionLocationAr":"طريق الدمام 2","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0},{"id":45,"shopBrnachId":301,"shopName":"AlBaik","shopNameAr":"البيك","shopDescription":"Broasted","shopDescriptionAr":"بروست","type":null,"typeAr":null,"distance":1.3815025196866604,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Logo2_636612266123864538.jpg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/62f1110c-0e7b-4e5d-9876-d1fd2c4ebf79/PIZZA0030.png","descriptionLocation":"yarmok","descriptionLocationAr":"اليرموك","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0},{"id":10418,"shopBrnachId":16672,"shopName":"Burgerina","shopNameAr":"برغرينا","shopDescription":"Fast Food Burger Restaurant","shopDescriptionAr":"مطعم برغر وجبات سريعة","type":null,"typeAr":null,"distance":1.6246798316746685,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/fbf5d84a-1abd-48fc-a346-cc5eca9a7c88/C8694EDF-24FF-4B33-8ED5-03B098E18540.jpeg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/280b6e72-3dca-42f0-a8ad-1dbe1e058b53/Untitled0design0060.png","descriptionLocation":"Qurtoba","descriptionLocationAr":"قرطبة","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":true,"dispatchType":0},{"id":40,"shopBrnachId":10991,"shopName":"Hardees","shopNameAr":"هارديز","shopDescription":"Fast Food","shopDescriptionAr":"وجبات سريعة","type":null,"typeAr":null,"distance":2.3624141271881673,"shopeRate":4,"isFavorite":false,"isOpen":false,"avatar":"http://dxs6hka2fuq9i.cloudfront.net/gogo_now/gogo_now/Hardees_Logo_V_TM_-__636675128508671913.jpg","headerImage":"https://fastersaimages.s3.eu-central-1.amazonaws.com/All/6ec20f65-8688-4327-8272-5664c2e76698/Untitled0design00200.png","descriptionLocation":"hard3","descriptionLocationAr":"هارد3","latitude":0,"longitude":0,"firstShiftAvailableTimeFrom":"12:00","firstShiftAvailableTimeTo":"23:59","secondShiftAvailableTimeFrom":null,"secondShiftAvailableTimeTo":null,"isBusy":false,"dispatchType":0}]
/// pageSize : 10
/// totalItemCount : 4
/// totalPagesCount : 1

class ReturnData {
  ReturnData({
      int? pageNumber, 
      List<Data>? data, 
      int? pageSize, 
      int? totalItemCount, 
      int? totalPagesCount,}){
    _pageNumber = pageNumber;
    _data = data;
    _pageSize = pageSize;
    _totalItemCount = totalItemCount;
    _totalPagesCount = totalPagesCount;
}

  ReturnData.fromJson(dynamic json) {
    _pageNumber = json['pageNumber'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
    _pageSize = json['pageSize'];
    _totalItemCount = json['totalItemCount'];
    _totalPagesCount = json['totalPagesCount'];
  }
  int? _pageNumber;
  List<Data>? _data;
  int? _pageSize;
  int? _totalItemCount;
  int? _totalPagesCount;

  int? get pageNumber => _pageNumber;
  List<Data>? get data => _data;
  int? get pageSize => _pageSize;
  int? get totalItemCount => _totalItemCount;
  int? get totalPagesCount => _totalPagesCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageNumber'] = _pageNumber;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['pageSize'] = _pageSize;
    map['totalItemCount'] = _totalItemCount;
    map['totalPagesCount'] = _totalPagesCount;
    return map;
  }

}

/// id : 36
/// shopBrnachId : 125
/// shopName : "McDonalds"
/// shopNameAr : "ماكدونالدز"
/// shopDescription : "Fast Food"
/// shopDescriptionAr : "وجبات سريعة"
/// type : null
/// typeAr : null
/// distance : 0.5387863417484943
/// shopeRate : 4
/// isFavorite : false
/// isOpen : false
/// avatar : "https://fastersaimages.s3.eu-central-1.amazonaws.com/All/788fa90e-84a8-45af-aff5-db28ea1550a3/mcdonalds-png-logo-picture-3.png"
/// headerImage : "https://fastersaimages.s3.eu-central-1.amazonaws.com/All/98ecd566-993b-43d1-945b-005d05722721/fasterImage/868.PNG"
/// descriptionLocation : "dammam rd 2"
/// descriptionLocationAr : "طريق الدمام 2"
/// latitude : 0
/// longitude : 0
/// firstShiftAvailableTimeFrom : "12:00"
/// firstShiftAvailableTimeTo : "23:59"
/// secondShiftAvailableTimeFrom : null
/// secondShiftAvailableTimeTo : null
/// isBusy : false
/// dispatchType : 0

class Data {
  Data({
      int? id, 
      int? shopBrnachId, 
      String? shopName, 
      String? shopNameAr, 
      String? shopDescription, 
      String? shopDescriptionAr, 
      dynamic type, 
      dynamic typeAr, 
      double? distance, 
      int? shopeRate, 
      bool? isFavorite, 
      bool? isOpen, 
      String? avatar, 
      String? headerImage, 
      String? descriptionLocation, 
      String? descriptionLocationAr, 
      int? latitude, 
      int? longitude, 
      String? firstShiftAvailableTimeFrom, 
      String? firstShiftAvailableTimeTo, 
      dynamic secondShiftAvailableTimeFrom, 
      dynamic secondShiftAvailableTimeTo, 
      bool? isBusy, 
      int? dispatchType,}){
    _id = id;
    _shopBrnachId = shopBrnachId;
    _shopName = shopName;
    _shopNameAr = shopNameAr;
    _shopDescription = shopDescription;
    _shopDescriptionAr = shopDescriptionAr;
    _type = type;
    _typeAr = typeAr;
    _distance = distance;
    _shopeRate = shopeRate;
    _isFavorite = isFavorite;
    _isOpen = isOpen;
    _avatar = avatar;
    _headerImage = headerImage;
    _descriptionLocation = descriptionLocation;
    _descriptionLocationAr = descriptionLocationAr;
    _latitude = latitude;
    _longitude = longitude;
    _firstShiftAvailableTimeFrom = firstShiftAvailableTimeFrom;
    _firstShiftAvailableTimeTo = firstShiftAvailableTimeTo;
    _secondShiftAvailableTimeFrom = secondShiftAvailableTimeFrom;
    _secondShiftAvailableTimeTo = secondShiftAvailableTimeTo;
    _isBusy = isBusy;
    _dispatchType = dispatchType;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _shopBrnachId = json['shopBrnachId'];
    _shopName = json['shopName'];
    _shopNameAr = json['shopNameAr'];
    _shopDescription = json['shopDescription'];
    _shopDescriptionAr = json['shopDescriptionAr'];
    _type = json['type'];
    _typeAr = json['typeAr'];
    _distance = json['distance'];
    _shopeRate = json['shopeRate'];
    _isFavorite = json['isFavorite'];
    _isOpen = json['isOpen'];
    _avatar = json['avatar'];
    _headerImage = json['headerImage'];
    _descriptionLocation = json['descriptionLocation'];
    _descriptionLocationAr = json['descriptionLocationAr'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _firstShiftAvailableTimeFrom = json['firstShiftAvailableTimeFrom'];
    _firstShiftAvailableTimeTo = json['firstShiftAvailableTimeTo'];
    _secondShiftAvailableTimeFrom = json['secondShiftAvailableTimeFrom'];
    _secondShiftAvailableTimeTo = json['secondShiftAvailableTimeTo'];
    _isBusy = json['isBusy'];
    _dispatchType = json['dispatchType'];
  }
  int? _id;
  int? _shopBrnachId;
  String? _shopName;
  String? _shopNameAr;
  String? _shopDescription;
  String? _shopDescriptionAr;
  dynamic _type;
  dynamic _typeAr;
  double? _distance;
  int? _shopeRate;
  bool? _isFavorite;
  bool? _isOpen;
  String? _avatar;
  String? _headerImage;
  String? _descriptionLocation;
  String? _descriptionLocationAr;
  int? _latitude;
  int? _longitude;
  String? _firstShiftAvailableTimeFrom;
  String? _firstShiftAvailableTimeTo;
  dynamic _secondShiftAvailableTimeFrom;
  dynamic _secondShiftAvailableTimeTo;
  bool? _isBusy;
  int? _dispatchType;

  int? get id => _id;
  int? get shopBrnachId => _shopBrnachId;
  String? get shopName => _shopName;
  String? get shopNameAr => _shopNameAr;
  String? get shopDescription => _shopDescription;
  String? get shopDescriptionAr => _shopDescriptionAr;
  dynamic get type => _type;
  dynamic get typeAr => _typeAr;
  double? get distance => _distance;
  int? get shopeRate => _shopeRate;
  bool? get isFavorite => _isFavorite;
  bool? get isOpen => _isOpen;
  String? get avatar => _avatar;
  String? get headerImage => _headerImage;
  String? get descriptionLocation => _descriptionLocation;
  String? get descriptionLocationAr => _descriptionLocationAr;
  int? get latitude => _latitude;
  int? get longitude => _longitude;
  String? get firstShiftAvailableTimeFrom => _firstShiftAvailableTimeFrom;
  String? get firstShiftAvailableTimeTo => _firstShiftAvailableTimeTo;
  dynamic get secondShiftAvailableTimeFrom => _secondShiftAvailableTimeFrom;
  dynamic get secondShiftAvailableTimeTo => _secondShiftAvailableTimeTo;
  bool? get isBusy => _isBusy;
  int? get dispatchType => _dispatchType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['shopBrnachId'] = _shopBrnachId;
    map['shopName'] = _shopName;
    map['shopNameAr'] = _shopNameAr;
    map['shopDescription'] = _shopDescription;
    map['shopDescriptionAr'] = _shopDescriptionAr;
    map['type'] = _type;
    map['typeAr'] = _typeAr;
    map['distance'] = _distance;
    map['shopeRate'] = _shopeRate;
    map['isFavorite'] = _isFavorite;
    map['isOpen'] = _isOpen;
    map['avatar'] = _avatar;
    map['headerImage'] = _headerImage;
    map['descriptionLocation'] = _descriptionLocation;
    map['descriptionLocationAr'] = _descriptionLocationAr;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['firstShiftAvailableTimeFrom'] = _firstShiftAvailableTimeFrom;
    map['firstShiftAvailableTimeTo'] = _firstShiftAvailableTimeTo;
    map['secondShiftAvailableTimeFrom'] = _secondShiftAvailableTimeFrom;
    map['secondShiftAvailableTimeTo'] = _secondShiftAvailableTimeTo;
    map['isBusy'] = _isBusy;
    map['dispatchType'] = _dispatchType;
    return map;
  }

}