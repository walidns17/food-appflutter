/// priceFrom : 0
/// priceTo : 0
/// distance : 0
/// isValidDistance : true
/// deliveryTimeInMinutesFrom : 0
/// deliveryTimeInMinutesTo : 0

class ApproxPrice {
  ApproxPrice({
      int? priceFrom, 
      int? priceTo, 
      int? distance, 
      bool? isValidDistance, 
      int? deliveryTimeInMinutesFrom, 
      int? deliveryTimeInMinutesTo,}){
    _priceFrom = priceFrom;
    _priceTo = priceTo;
    _distance = distance;
    _isValidDistance = isValidDistance;
    _deliveryTimeInMinutesFrom = deliveryTimeInMinutesFrom;
    _deliveryTimeInMinutesTo = deliveryTimeInMinutesTo;
}

  ApproxPrice.fromJson(dynamic json) {
    _priceFrom = json['priceFrom'];
    _priceTo = json['priceTo'];
    _distance = json['distance'];
    _isValidDistance = json['isValidDistance'];
    _deliveryTimeInMinutesFrom = json['deliveryTimeInMinutesFrom'];
    _deliveryTimeInMinutesTo = json['deliveryTimeInMinutesTo'];
  }
  int? _priceFrom;
  int? _priceTo;
  int? _distance;
  bool? _isValidDistance;
  int? _deliveryTimeInMinutesFrom;
  int? _deliveryTimeInMinutesTo;

  int? get priceFrom => _priceFrom;
  int? get priceTo => _priceTo;
  int? get distance => _distance;
  bool? get isValidDistance => _isValidDistance;
  int? get deliveryTimeInMinutesFrom => _deliveryTimeInMinutesFrom;
  int? get deliveryTimeInMinutesTo => _deliveryTimeInMinutesTo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['priceFrom'] = _priceFrom;
    map['priceTo'] = _priceTo;
    map['distance'] = _distance;
    map['isValidDistance'] = _isValidDistance;
    map['deliveryTimeInMinutesFrom'] = _deliveryTimeInMinutesFrom;
    map['deliveryTimeInMinutesTo'] = _deliveryTimeInMinutesTo;
    return map;
  }

}