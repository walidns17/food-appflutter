/// isSucceeded : true
/// apiStatusCode : 200
/// errorMessage : ""
/// returnData : {"pageNumber":0,"data":[{"id":1,"address":"okaz","street":"ali","descriptionLocation":"home","latitude":24.7186502,"longitude":46.6648383,"zipCode":"12341"},{"id":3,"address":"محطة كهرباء حي قرطبة، 3690 طريق الأمير محمد بن سلمان بن عبدالعزيز، 6554، قرطبة، الرياض 13248، السعودية","street":"شارع البيضاء ","descriptionLocation":"البيت","latitude":24.8214622,"longitude":46.742177,"zipCode":"13248"},{"id":4,"address":"محطة كهرباء حي قرطبة، 3888 طريق الأمير محمد بن سلمان بن عبدالعزيز، 6648، قرطبة، الرياض 13248، السعودية","street":"البيضاء ","descriptionLocation":"المكتب","latitude":24.822316200000003,"longitude":46.7441403,"zipCode":"13248"},{"id":6,"address":"3340 طريق الملك فيصل، بني خدرة، المدينة المنورة 42311، السعودية","street":"طريق الملك فيصل","descriptionLocation":"البيك المدينة","latitude":24.463953684668123,"longitude":39.61472276598215,"zipCode":"112233"},{"id":7,"address":"DMUF3619، 3619 ابو بكر عبدالله، 8871، حي الروابي، المدينة المنورة 42382، السعودية","street":"اسم الطريق","descriptionLocation":"تيست الشوب","latitude":25.1226414,"longitude":46.6706917,"zipCode":null},{"id":8,"address":"RUQA3900، 3900 عبدالله بن إبراهيم بن سيف، 7845، حي قرطبة، الرياض 13248، السعودية","street":"البيضاء ","descriptionLocation":"مسجد","latitude":24.8331205,"longitude":46.744415499999995,"zipCode":"13248"},{"id":9,"address":"RUQD3385، 3385 مرات، 8093، حي قرطبة، الرياض 13245، السعودية","street":"البيضاء ","descriptionLocation":"مسجد ٢","latitude":24.818976,"longitude":46.7456391,"zipCode":"13245"},{"id":10,"address":"RUQD3385، 3385 مرات، 8093، حي قرطبة، الرياض 132d45، السعودية","street":"البيضاء ","descriptionLocation":"مسجد44٢","latitude":24.818974,"longitude":46.7456391,"zipCode":"13245"},{"id":11,"address":"RUQC3429، 3429 طريق الامير سعود بن محمد بن مقرن الفرعي، 8356، حي قرطبة، الرياض 13245، السعودية","street":"البيضاء ","descriptionLocation":"مسجد ٦","latitude":24.821314899999997,"longitude":46.746233,"zipCode":"13245"},{"id":12,"address":"RUME2899، 2899 ابن ابي حفصه، 6285، حي المونسية، الرياض 13421، السعودية","street":"اياتاب","descriptionLocation":"علي","latitude":24.840583799999997,"longitude":46.744165800000005,"zipCode":"13421"}],"pageSize":0,"totalItemCount":31,"totalPagesCount":4}

class UserAddress {
  UserAddress({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      ReturnData? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  UserAddress.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    _returnData = json['returnData'] != null ? ReturnData.fromJson(json['returnData']) : null;
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  ReturnData? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  ReturnData? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.toJson();
    }
    return map;
  }

}

/// pageNumber : 0
/// data : [{"id":1,"address":"okaz","street":"ali","descriptionLocation":"home","latitude":24.7186502,"longitude":46.6648383,"zipCode":"12341"},{"id":3,"address":"محطة كهرباء حي قرطبة، 3690 طريق الأمير محمد بن سلمان بن عبدالعزيز، 6554، قرطبة، الرياض 13248، السعودية","street":"شارع البيضاء ","descriptionLocation":"البيت","latitude":24.8214622,"longitude":46.742177,"zipCode":"13248"},{"id":4,"address":"محطة كهرباء حي قرطبة، 3888 طريق الأمير محمد بن سلمان بن عبدالعزيز، 6648، قرطبة، الرياض 13248، السعودية","street":"البيضاء ","descriptionLocation":"المكتب","latitude":24.822316200000003,"longitude":46.7441403,"zipCode":"13248"},{"id":6,"address":"3340 طريق الملك فيصل، بني خدرة، المدينة المنورة 42311، السعودية","street":"طريق الملك فيصل","descriptionLocation":"البيك المدينة","latitude":24.463953684668123,"longitude":39.61472276598215,"zipCode":"112233"},{"id":7,"address":"DMUF3619، 3619 ابو بكر عبدالله، 8871، حي الروابي، المدينة المنورة 42382، السعودية","street":"اسم الطريق","descriptionLocation":"تيست الشوب","latitude":25.1226414,"longitude":46.6706917,"zipCode":null},{"id":8,"address":"RUQA3900، 3900 عبدالله بن إبراهيم بن سيف، 7845، حي قرطبة، الرياض 13248، السعودية","street":"البيضاء ","descriptionLocation":"مسجد","latitude":24.8331205,"longitude":46.744415499999995,"zipCode":"13248"},{"id":9,"address":"RUQD3385، 3385 مرات، 8093، حي قرطبة، الرياض 13245، السعودية","street":"البيضاء ","descriptionLocation":"مسجد ٢","latitude":24.818976,"longitude":46.7456391,"zipCode":"13245"},{"id":10,"address":"RUQD3385، 3385 مرات، 8093، حي قرطبة، الرياض 132d45، السعودية","street":"البيضاء ","descriptionLocation":"مسجد44٢","latitude":24.818974,"longitude":46.7456391,"zipCode":"13245"},{"id":11,"address":"RUQC3429، 3429 طريق الامير سعود بن محمد بن مقرن الفرعي، 8356، حي قرطبة، الرياض 13245، السعودية","street":"البيضاء ","descriptionLocation":"مسجد ٦","latitude":24.821314899999997,"longitude":46.746233,"zipCode":"13245"},{"id":12,"address":"RUME2899، 2899 ابن ابي حفصه، 6285، حي المونسية، الرياض 13421، السعودية","street":"اياتاب","descriptionLocation":"علي","latitude":24.840583799999997,"longitude":46.744165800000005,"zipCode":"13421"}]
/// pageSize : 0
/// totalItemCount : 31
/// totalPagesCount : 4

class ReturnData {
  ReturnData({
      int? pageNumber, 
      List<Data>? data, 
      int? pageSize, 
      int? totalItemCount, 
      int? totalPagesCount,}){
    _pageNumber = pageNumber;
    _data = data;
    _pageSize = pageSize;
    _totalItemCount = totalItemCount;
    _totalPagesCount = totalPagesCount;
}

  ReturnData.fromJson(dynamic json) {
    _pageNumber = json['pageNumber'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
    _pageSize = json['pageSize'];
    _totalItemCount = json['totalItemCount'];
    _totalPagesCount = json['totalPagesCount'];
  }
  int? _pageNumber;
  List<Data>? _data;
  int? _pageSize;
  int? _totalItemCount;
  int? _totalPagesCount;

  int? get pageNumber => _pageNumber;
  List<Data>? get data => _data;
  int? get pageSize => _pageSize;
  int? get totalItemCount => _totalItemCount;
  int? get totalPagesCount => _totalPagesCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageNumber'] = _pageNumber;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['pageSize'] = _pageSize;
    map['totalItemCount'] = _totalItemCount;
    map['totalPagesCount'] = _totalPagesCount;
    return map;
  }

}

/// id : 1
/// address : "okaz"
/// street : "ali"
/// descriptionLocation : "home"
/// latitude : 24.7186502
/// longitude : 46.6648383
/// zipCode : "12341"

class Data {
  Data({
      int? id, 
      String? address, 
      String? street, 
      String? descriptionLocation, 
      double? latitude, 
      double? longitude, 
      String? zipCode,}){
    _id = id;
    _address = address;
    _street = street;
    _descriptionLocation = descriptionLocation;
    _latitude = latitude;
    _longitude = longitude;
    _zipCode = zipCode;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _address = json['address'];
    _street = json['street'];
    _descriptionLocation = json['descriptionLocation'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _zipCode = json['zipCode'];
  }
  int? _id;
  String? _address;
  String? _street;
  String? _descriptionLocation;
  double? _latitude;
  double? _longitude;
  String? _zipCode;

  int? get id => _id;
  String? get address => _address;
  String? get street => _street;
  String? get descriptionLocation => _descriptionLocation;
  double? get latitude => _latitude;
  double? get longitude => _longitude;
  String? get zipCode => _zipCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['address'] = _address;
    map['street'] = _street;
    map['descriptionLocation'] = _descriptionLocation;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['zipCode'] = _zipCode;
    return map;
  }

}