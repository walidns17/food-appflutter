/// isSucceeded : true
/// apiStatusCode : 200
/// errorMessage : ""
/// returnData : {"priceFrom":0.0,"priceTo":0.0,"distance":0.0,"isValidDistance":true,"deliveryTimeInMinutesFrom":0.0,"deliveryTimeInMinutesTo":0.0}

class ApproxPrice {
  ApproxPrice({
      bool? isSucceeded, 
      int? apiStatusCode, 
      String? errorMessage, 
      ReturnData? returnData,}){
    _isSucceeded = isSucceeded;
    _apiStatusCode = apiStatusCode;
    _errorMessage = errorMessage;
    _returnData = returnData;
}

  ApproxPrice.fromJson(dynamic json) {
    _isSucceeded = json['isSucceeded'];
    _apiStatusCode = json['apiStatusCode'];
    _errorMessage = json['errorMessage'];
    _returnData = json['returnData'] != null ? ReturnData.fromJson(json['returnData']) : null;
  }
  bool? _isSucceeded;
  int? _apiStatusCode;
  String? _errorMessage;
  ReturnData? _returnData;

  bool? get isSucceeded => _isSucceeded;
  int? get apiStatusCode => _apiStatusCode;
  String? get errorMessage => _errorMessage;
  ReturnData? get returnData => _returnData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isSucceeded'] = _isSucceeded;
    map['apiStatusCode'] = _apiStatusCode;
    map['errorMessage'] = _errorMessage;
    if (_returnData != null) {
      map['returnData'] = _returnData?.toJson();
    }
    return map;
  }

}

/// priceFrom : 0.0
/// priceTo : 0.0
/// distance : 0.0
/// isValidDistance : true
/// deliveryTimeInMinutesFrom : 0.0
/// deliveryTimeInMinutesTo : 0.0

class ReturnData {
  ReturnData({
      double? priceFrom, 
      double? priceTo, 
      double? distance, 
      bool? isValidDistance, 
      double? deliveryTimeInMinutesFrom, 
      double? deliveryTimeInMinutesTo,}){
    _priceFrom = priceFrom;
    _priceTo = priceTo;
    _distance = distance;
    _isValidDistance = isValidDistance;
    _deliveryTimeInMinutesFrom = deliveryTimeInMinutesFrom;
    _deliveryTimeInMinutesTo = deliveryTimeInMinutesTo;
}

  ReturnData.fromJson(dynamic json) {
    _priceFrom = json['priceFrom'];
    _priceTo = json['priceTo'];
    _distance = json['distance'];
    _isValidDistance = json['isValidDistance'];
    _deliveryTimeInMinutesFrom = json['deliveryTimeInMinutesFrom'];
    _deliveryTimeInMinutesTo = json['deliveryTimeInMinutesTo'];
  }
  double? _priceFrom;
  double? _priceTo;
  double? _distance;
  bool? _isValidDistance;
  double? _deliveryTimeInMinutesFrom;
  double? _deliveryTimeInMinutesTo;

  double? get priceFrom => _priceFrom;
  double? get priceTo => _priceTo;
  double? get distance => _distance;
  bool? get isValidDistance => _isValidDistance;
  double? get deliveryTimeInMinutesFrom => _deliveryTimeInMinutesFrom;
  double? get deliveryTimeInMinutesTo => _deliveryTimeInMinutesTo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['priceFrom'] = _priceFrom;
    map['priceTo'] = _priceTo;
    map['distance'] = _distance;
    map['isValidDistance'] = _isValidDistance;
    map['deliveryTimeInMinutesFrom'] = _deliveryTimeInMinutesFrom;
    map['deliveryTimeInMinutesTo'] = _deliveryTimeInMinutesTo;
    return map;
  }

}