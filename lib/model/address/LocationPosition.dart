/// latitude : 0.0
/// longitude : 0.0
/// locationName : ""
/// locality : ""
/// country : ""
/// isValidDistance : true

class LocationPosition {
  LocationPosition({
      double? latitude, 
      double? longitude, 
      String? locationName, 
      String? locality, 
      String? country, 
      bool? isValidDistance,}){
    _latitude = latitude;
    _longitude = longitude;
    _locationName = locationName;
    _locality = locality;
    _country = country;
    _isValidDistance = isValidDistance;
}

  LocationPosition.fromJson(dynamic json) {
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _locationName = json['locationName'];
    _locality = json['locality'];
    _country = json['country'];
    _isValidDistance = json['isValidDistance'];
  }
  double? _latitude;
  double? _longitude;
  String? _locationName;
  String? _locality;
  String? _country;
  bool? _isValidDistance;

  double? get latitude => _latitude;
  double? get longitude => _longitude;
  String? get locationName => _locationName;
  String? get locality => _locality;
  String? get country => _country;
  bool? get isValidDistance => _isValidDistance;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['locationName'] = _locationName;
    map['locality'] = _locality;
    map['country'] = _country;
    map['isValidDistance'] = _isValidDistance;
    return map;
  }

}