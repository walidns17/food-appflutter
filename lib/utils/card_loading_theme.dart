
import 'dart:ui';


class CardLoadingTheme {
  /// at the beginning this will be the [Color] for the background [CardLoading]
  /// default Color(0xFFE5E5E5)
  final Color colorOne;

  /// at the beginning this will be the [Color] for the Loading [CardLoading]
  /// default Color(0xFFF0F0F0)
  final Color colorTwo;

  /// {@macro card_loading_theme}
  const CardLoadingTheme({
    required this.colorOne,
    required this.colorTwo,
  });

  static const CardLoadingTheme defaultTheme = CardLoadingTheme(
    colorOne: const Color(0xFFE5E5E5),
    colorTwo: const Color(0xFFF0F0F0),
  );
}