import 'package:hexcolor/hexcolor.dart';

class GlobalColors {
  static HexColor headerColors = HexColor('#0C4A7A');
  static HexColor buttonColors = HexColor('#002A5A ');

  static HexColor kBgColor = HexColor('e7ded7');
  static HexColor kGreyColor = HexColor('dcdde2');
  static HexColor kSmProductBgColor = HexColor('f9f9f9');

}
