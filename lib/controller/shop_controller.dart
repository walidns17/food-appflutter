import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:food/model/address/ApproxPrice.dart';
import 'package:food/model/shop/ProductModel.dart';
import 'package:food/model/shop/ProductsByFoodType.dart';
import 'package:food/model/shop/ShopMenue.dart';
import 'package:food/model/shop/ShopSearch.dart';
import 'package:food/server/api_config.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ShopController extends GetxController {
  static const storage = FlutterSecureStorage();

  Future<ShopSearch> getShopsSearch(shopTypeId) async {
    final coordinates = await _getCoordinatesFromStorage();
    final response = await _getShopData(
        "${ApiConfig.baseUrl}/Shop/Search?ShopTypeId=$shopTypeId&lan=${coordinates['longitude']}&lat=${coordinates['latitude']}");
    return ShopSearch.fromJson(response);
  }

  Future<ProductsByFoodType> getProductsByFoodType(foodTypeId) async {
    final coordinates = await _getCoordinatesFromStorage();
    final response = await _getShopData(
        "${ApiConfig.baseUrl}/Shop/SearchProductsByFoodType?FoodTypeId=$foodTypeId&lan=${coordinates['longitude']}&lat=${coordinates['latitude']}");
    return ProductsByFoodType.fromJson(response);
  }

  Future<ShopMenue> getShopMenue(shopId) async {
    final response = await _getShopData(
        "${ApiConfig.baseUrl}/Shop/ShopMenue?shopId=$shopId");

    return ShopMenue.fromJson(response);
  }

  Future<ApproxPrice> getApproxPrice(shopBranchId) async {
    final coordinates = await _getCoordinatesFromStorage();
    final loginModel = {
      "customerId": 0,
      "customerLongitude": coordinates['longitude'],
      "customerLatitude": coordinates['latitude'],
      "shopBranchId": shopBranchId
    };
    final response = await _postShopData(
      '${ApiConfig.baseUrl}/DeliveryOrder/GetApproxPrice',
      loginModel,
    );
    return ApproxPrice.fromJson(response);
  }

  Future<ProductModel> getProductItem(productTypeId) async {

    final response = await _getShopData(
        "${ApiConfig.baseUrl}/Product/ProductType/$productTypeId");
    return ProductModel.fromJson(response);
  }

  Future<Map<String, dynamic>> _getCoordinatesFromStorage() async {
    final latitude = await storage.read(key: "latitude");
    final longitude = await storage.read(key: "longitude");
    return {
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  Future<dynamic> _getShopData(String url) async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }

  Future<dynamic> _postShopData(String url, dynamic data) async {
    final body = json.encode(data);
    final response = await http.post(
      Uri.parse(url),
      headers: {"Content-Type": "application/json"},
      body: body,
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }
}
