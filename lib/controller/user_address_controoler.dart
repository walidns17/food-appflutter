import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:food/components/toast.dart';
import 'package:food/model/address/UserAddress.dart';
import 'package:food/server/api_config.dart';
import 'package:food/view/home/pages/home.dart';
import 'package:food/view/map/map_page.dart';
import 'package:food/widgets/button.global.dart';
import 'package:food/widgets/home/list_of_user_address.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:overlay_loading_progress/overlay_loading_progress.dart';
import 'package:http/http.dart' as http;

class UserAddressController extends GetxController {
  static const storage = FlutterSecureStorage();


  Future<String> getAddressLocation() async {
    await Future.delayed(const Duration(seconds: 1));
    String? address = await storage.read(key: "address");
    if (address != null) {
      return address;
    } else {
      return "Location not available";
    }
  }

  Future<LatLng> getLoadLocation() async {
    await Future.delayed(const Duration(seconds: 1));
    double? longitude = await storage.read(key: "longitude") as double?;
    double? latitude = await storage.read(key: "latitude") as double?;

    if (longitude != null && latitude != null) {
      return LatLng(latitude, longitude);
    } else {
      return LatLng(24.707510, 46.696247);
    }
  }

  Future<String> getLocationAndAddress() async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    List<Placemark> placemarks = await placemarkFromCoordinates(
      position.latitude,
      position.longitude,
    );
    Placemark placemark = placemarks[0];

    double latitude = position.latitude;
    double longitude = position.longitude;

    String? locationName = placemark.name; // اسم الموقع
    String? locality = placemark.locality; // المدينة
    String? country = placemark.country; // البلد


    //
    // ApproxPrice approxPrice = await getGetApproxPrice(0, longitude, latitude, 0);
    // bool? isValidDistance = approxPrice.returnData!.isValidDistance;
    //
    // LocationPosition locationPosition = LocationPosition.fromJson(LocationPosition(country:country,
    //     locationName:  locationName,
    //     locality: locality,isValidDistance: isValidDistance,longitude: longitude,latitude:latitude));

    return "$locationName,$locality";


  }

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    permission = await Geolocator.requestPermission();

    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are denied');
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low);
    print(position);

    return await Geolocator.getCurrentPosition();
  }

  Future<UserAddress> getUserAddress() async {
    String? token = await storage.read(key: "token");

    http.Response response = await http.get(
      Uri.parse('${ApiConfig.baseUrl}/UserAddress'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);

      UserAddress userAddress = UserAddress.fromJson(jsonResponse);

      return userAddress;
    } else {

      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }

  Future<String> postUserAddress(address, street, descriptionLocation, latitude, longitude, zipCode, context) async {

    OverlayLoadingProgress.start(context, gifOrImagePath: 'assets/gif/loading.gif', barrierDismissible: true, widget: Container(
      height: 100,
      width: 100,
      color: Colors.black38,
      child: const Center(child: CircularProgressIndicator()),
    ));


    if (address == "") {
      OverlayLoadingProgress.stop();
      Toast.motionToastError(context, "Some required fields are missing or empty");
    }

    String? token = await storage.read(key: "token");
    if (token == null) {
      // يمكنك هنا التعامل مع فقدان الرمز (token)
      OverlayLoadingProgress.stop();
      return "User token is missing or empty";
    }

    final userAddressModel = {
      "id": 0,
      "address": address,
      "street": street,
      "descriptionLocation": descriptionLocation,
      "latitude": latitude,
      "longitude": longitude,
      "zipCode": zipCode
    };

    String body = json.encode(userAddressModel);
    http.Response response = await http.post(
      Uri.parse('${ApiConfig.baseUrl}/UserAddress'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: body,
    );

    OverlayLoadingProgress.stop();

    if (response.statusCode == 200) {
      storageAddress(address, latitude, longitude, context);
      return "Success";
    } else {
      Toast.motionToastError(context, jsonDecode(response.body)["Message"] ?? "Unknown Error Occurred");
      return "Request failed";
    }
  }
  //***

  Future<void> showAddressDialog(listOfUserAddress, userAddress, context) async {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      useSafeArea: true,
      builder: (context) {
        return Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Padding(
                  padding: EdgeInsets.all(14.0),
                  child: Text(
                    "Choose from saved addresses",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: GestureDetector(
                    child: const Icon(Icons.close),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: const Color(0XFFE6F7F4),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Icon(Icons.refresh),
                  const SizedBox(width: 10),
                  FutureBuilder<String>(
                    future: userAddress,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      }
                      return Text(
                        snapshot.data ?? "",
                        style: const TextStyle(color: Color(0xFFF7B733)),
                      );
                    },
                  ),
                ],
              ),
            ),
            ListOfUserAddress(listOfUserAddress: listOfUserAddress),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: SizedBox(
                height: 50,
                width: double.infinity,
                child: ButtonGlobal(
                  onPress: () {
                    Navigator.pop(context);
                    Get.to(const MapPage());
                  },
                  text: "Add Address",
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void storageAddress(address,latitude,longitude,context){
    storage.write(key: "address", value: address);
    storage.write(key: "latitude", value: latitude);
    storage.write(key: "longitude", value: longitude);

    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 500),
        pageBuilder: (context, animation, secondaryAnimation) {
          return const Home();
        },
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(1.0, 0.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          var offsetAnimation = animation.drive(tween);
          return SlideTransition(position: offsetAnimation, child: child);
        },
      ),
          (route) => false,
    );
  }

}
