import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:food/model/home/Popular.dart';
import 'package:food/model/home/ShopType.dart';
import 'package:food/server/api_config.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;

import '../model/home/FoodType.dart';

class HomeController extends GetxController {
  var isLoading = false.obs;
  static const storage = FlutterSecureStorage();


  Future<List> getBanner() async {
    http.Response response;
    response = await http
        .get(Uri.parse("${ApiConfig.baseUrl}/Banner/getBanner"));
    // ignore: unnecessary_null_comparison

    Map mapResponse = json.decode(response.body);
    return mapResponse['data'] as List;
  }

  Future<FoodType> getFoodType() async {
    isLoading(true);

    http.Response response =
        await http.get(Uri.parse('${ApiConfig.baseUrl}/FoodType'));

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body.toString());

      FoodType foodType = FoodType.fromJson(jsonResponse);

      return foodType;
    } else {
      isLoading(false);

      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }

  Future<ShopType> getShopType() async {
    isLoading(true);

    http.Response response =
        await http.get(Uri.parse('${ApiConfig.baseUrl}/ShopType'));

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body.toString());

      ShopType shopType = ShopType.fromJson(jsonResponse);

      return shopType;
    } else {
      isLoading(false);

      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }

  Future<Popular> getPopular() async {
    isLoading(true);

    http.Response response = await http
        .get(Uri.parse('${ApiConfig.baseUrl}/Shop/Popular'));

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);

      Popular popular = Popular.fromJson(jsonResponse);

      return popular;
    } else {
      isLoading(false);

      throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
    }
  }


}
