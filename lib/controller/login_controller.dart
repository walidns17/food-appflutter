import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:food/model/loginModel.dart';
import 'package:food/model/profileModel.dart';
import 'package:food/view/identity/_otp_verify.dart';
import 'package:food/view/identity/editProfile.dart';
import 'package:food/view/splash.view.dart';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../server/api_config.dart';

class LoginController extends GetxController {
  var isLoading = false.obs;
  static const storage = FlutterSecureStorage();

  Future<void> getLogin(phoneNumber, context) async {
    try {
      isLoading(true);
      final response = await http.get(
        Uri.parse(
            '${ApiConfig.baseUrl}/customer/identity/login/$phoneNumber'),
      );

      if (response.statusCode == 200) {
        Get.offAll((OTPVerify(phoneNumber: phoneNumber)));
      } else {
        throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
      }
    } catch (e) {
      isLoading(false);
      showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: const Text(""),
              contentPadding: const EdgeInsets.all(20),
              children: [Text(e.toString())],
            );
          });
    }
  }

  Future<LoginModel?> getOTP(String otp, String phoneNumber, double latitude,
      double longitude, context) async {
    try {
      isLoading(true);

      final loginModel = {
        "otp": otp,
        "phoneNumber": phoneNumber,
        "latitude": latitude,
        "longitude": longitude,
        "referrerId": 0
      };

      String body = json.encode(loginModel);

      http.Response response = await http.post(
        Uri.parse('${ApiConfig.baseUrl}/customer/identity/login'),
        headers: {"Content-Type": "application/json"},
        body: body,
      );

      if (response.statusCode == 200) {
        LoginModel model = LoginModel.fromJson(json.decode(response.body));
        final String token = model.returnData!.token!;

        storage.write(key: 'token', value: token);

        getProfile(token);

      } else {
        throw jsonDecode(response.body)["Message"] ?? "Unknown Error Occured";
      }
    } catch (e) {
      isLoading(false);
      showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: const Text(""),
              contentPadding: const EdgeInsets.all(20),
              children: [Text(e.toString())],
            );
          });
    }
  }

  Future<ProfileModel?> getProfile(String token) async {
    try {
      http.Response response = await http.get(
        Uri.parse(
            '${ApiConfig.baseUrl}/customer/identity/getProfile'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        },
      );

      if (response.statusCode == 200) {
        ProfileModel model = ProfileModel.fromJson(json.decode(response.body));

        final String fullname = model.returnData!.fullname!;
        final String email = model.returnData!.email!;
        final String imagePath = model.returnData!.imagePath!;

        if (model.returnData!.fullname == "" ||
            model.returnData!.fullname == "new user") {
          Get.offAll(EditProfilePage(
              fullname: fullname, email: email, imagePath: imagePath));
        } else {
          Get.offAll((const SplashView()));
        }
      } else {
        return null;
      }
    } catch (e) {
      print(e);
    }
  }


}
