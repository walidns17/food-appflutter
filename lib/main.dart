import 'package:flutter/material.dart';
import 'package:food/components/cart_provider.dart';
import 'package:food/view/splash.view.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      create: (context) => CartProvider(),
      child:  GetMaterialApp(
        theme: ThemeData(
          fontFamily: 'cairo',
        ),
        debugShowCheckedModeBanner: false,

        home: const SplashView(),
      ),
    );
  }
}
