import 'package:flutter/material.dart';
import 'package:food/model/cart_Item.dart';
import 'package:provider/provider.dart';

class CartProvider extends ChangeNotifier {
  static CartProvider? _instance; // تعريف متغير ستاتيك

  static CartProvider get instance {
    // توفير إصدار واحد فقط من CartProvider
    _instance ??= CartProvider();
    return _instance!;
  }

  final List<Cart> _cartItems = [];

  List<Cart> get cartItems => _cartItems;

  void addToCart(Cart item) {

    final existingItemIndex = _cartItems.indexWhere(
      (cartItem) => _areItemsEqual(cartItem, item),
    );

    if (existingItemIndex != -1) {
      _cartItems[existingItemIndex].updateData(item);
    } else {
      _cartItems.add(item);
    }
    notifyListeners();
  }

    bool _areItemsEqual(Cart item1, Cart item2) {
      // قارن بناءً على خصائص محددة
      final areAdditionalComponentCartsEqual =
          item1.additionalComponentCarts.length ==
                  item2.additionalComponentCarts.length &&
              item1.additionalComponentCarts.every((component1) => item2
                  .additionalComponentCarts
                  .any((component2) => component1 == component2));

      return areAdditionalComponentCartsEqual &&
          item1.productDetailsSizeId == item2.productDetailsSizeId;
    }

  void removeFromCart(Cart item) {
    _cartItems.remove(item);

    notifyListeners();
  }

  void clearCart() {
    _cartItems.clear();
    notifyListeners();
  }

  double sumPrice() {
    double total = 0.0;
    for (var component in _cartItems) {
      total += component.price;
    }
    return total;
  }

  // Cart? getCartByProductId(int productId) {
  //   // قم بالبحث في القائمة `_cartItems` للعنصر الذي يتوافق مع معرف المنتج
  //   final cartItem = _cartItems.firstWhere(
  //         (cart) => cart.productId == productId,
  //     orElse: () => null!,
  //   );
  //
  //   return cartItem;
  // }

}
