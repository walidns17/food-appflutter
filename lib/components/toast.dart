

import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';

///  //https://medium.flutterdevs.com/motion-toast-in-flutter-97dd381eb643
class Toast {

  static  motionToastError(context, text){
    return MotionToast(
      icon: Icons.error,
      primaryColor: Colors.red,
      description:
      Text(text),
    ).show(context);
  }

}

