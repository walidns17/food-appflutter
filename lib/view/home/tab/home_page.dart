import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/model/address/UserAddress.dart';
import 'package:food/model/home/FoodType.dart';
import 'package:food/model/home/Popular.dart';
import 'package:food/model/home/ShopType.dart';
import 'package:food/widgets/home/list_food_type.dart';
import 'package:food/widgets/home/list_of_banner.dart';
import 'package:food/widgets/home/list_of_popular.dart';
import 'package:food/widgets/home/list_of_user_address.dart';
import 'package:food/widgets/home/list_shop_type.dart';
import 'package:get/get.dart';

import '../../../controller/home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomeTab();
}

class _HomeTab extends State<HomePage> with SingleTickerProviderStateMixin {
  final _homeController = Get.put(HomeController());
  final _userAddressController = Get.put(UserAddressController());

  late TabController tabController;

  late Future<List> listOfBanner;
  late Future<FoodType> listOfFoodType;
  late Future<ShopType> listOfShopType;
  late Future<Popular> listOfPopular;
  late Future<UserAddress> listOfUserAddress;
  late Future<String> userAddress;


  @override
  void initState() {
    super.initState();

    tabController = TabController(length: 3, vsync: this);
    listOfBanner = _homeController.getBanner();
    listOfFoodType = _homeController.getFoodType();
    listOfShopType = _homeController.getShopType();
    listOfPopular = _homeController.getPopular();
    listOfUserAddress = _userAddressController.getUserAddress();
    userAddress =  _userAddressController.getLocationAndAddress();

  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            _appBar(),
            Expanded(
              child: SingleChildScrollView(
                physics: const ClampingScrollPhysics(),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(18, 5, 18, 5),
                      padding: EdgeInsets.all(15),
                      decoration: const BoxDecoration(
                        color: Color(0XFFE6F7F4),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              const Icon(Icons.share_location),
                              const SizedBox(width: 10),

                              FutureBuilder<String>(
                                future:Future.delayed(const Duration(seconds: 1), () {
                                  return _userAddressController.getAddressLocation();
                                }),
                                builder: (BuildContext context,
                                    AsyncSnapshot<String> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    // Return a loading indicator or placeholder widget
                                    return const Center(
                                        child: SizedBox(
                                            height: 15,
                                            width: 15,
                                            child:
                                                CircularProgressIndicator()));
                                  } else {
                                    return Text(
                                      snapshot.data.toString(),
                                      style: const TextStyle(
                                          color: Color(0xFFF7B733)),
                                    );
                                  }
                                },
                              )
                            ],
                          ),
                          GestureDetector(
                            child: const Center(
                              child: Text(
                                "Change Address",
                                style: TextStyle(color: Color(0xFF64C8B6)),
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                _userAddressController.showAddressDialog(listOfUserAddress, userAddress, context);
                              });
                            },
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 200,
                      width: double.infinity,
                      color: Colors.white,
                      child: ListOfBanner(listOfBanner: listOfBanner),
                    ),
                    Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: const Text("We can help you choose",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                              )),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: SizedBox(
                        child: Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: SizedBox(
                              child: Column(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.height,
                                    decoration: BoxDecoration(
                                        color: const Color(0XFFD5D8DC),
                                        borderRadius:
                                            BorderRadius.circular(16)),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: TabBar(
                                            unselectedLabelColor:
                                                Colors.black38,
                                            labelColor: Colors.black,
                                            indicatorColor: Colors.white,
                                            indicatorWeight: 2,
                                            indicator: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                            ),
                                            controller: tabController,
                                            tabs: const [
                                              Tab(
                                                text: 'By Category',
                                              ),
                                              Tab(
                                                text: 'By Craving',
                                              ),

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    height: 100, //This height will be ignored
                                    alignment: Alignment.topCenter,
                                    child: TabBarView(
                                      controller: tabController,
                                      children: [
                                        ListShopType(
                                            listOfShopType: listOfShopType),
                                        ListFoodType(
                                            listOfFoodType: listOfFoodType),

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: ListOfPopular(listOfPopular: listOfPopular),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _appBar() {
    return Container(
      color: const Color(0xFFFFFFFF),
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(10),
                  decoration: const BoxDecoration(
                    color: Color(0xffF9F9F9),
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            "What are you looking for?",
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              SizedBox(
                child: Image.asset(
                  'assets/images/logo.png',
                  height: 30,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }


}
