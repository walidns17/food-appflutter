import 'package:flutter/material.dart';
import 'package:food/constans/colors.dart';
import 'package:food/controller/shop_controller.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/model/address/UserAddress.dart';
import 'package:food/model/shop/ShopSearch.dart';
import 'package:food/utils/card_loading.dart';
import 'package:food/view/screen/404_error/not_found_error.dart';
import 'package:food/widgets/head_widget.dart';
import 'package:food/widgets/shop/shop_row_item_widger.dart';
import 'package:get/get.dart';

import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class ShopListPage extends StatefulWidget {
  const ShopListPage({Key? key, required this.id, required this.foodTypeName})
      : super(key: key);

  final String id;
  final String foodTypeName;

  @override
  State<ShopListPage> createState() => _ShopListPageState();
}

class _ShopListPageState extends State<ShopListPage> {
  final _userAddressController = Get.put(UserAddressController());
  final _shopController = Get.put(ShopController());

  late Future<String> userAddress;
  late Future<UserAddress> listOfUserAddress;
  late Future<ShopSearch> listShopsByFoodType;

  NumberFormat formatter = NumberFormat("0.00");

  bool myVariable = true; // يمكنك تغيير هذه القيمة

  @override
  void initState() {
    super.initState();
    userAddress = _userAddressController.getLocationAndAddress();
    listOfUserAddress = _userAddressController.getUserAddress();

    listShopsByFoodType = _shopController.getShopsSearch(
        widget.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(children: [
        HeadWidget(
            text: widget.foodTypeName,
            userAddressController: _userAddressController,
            listOfUserAddress: listOfUserAddress,
            userAddress: userAddress),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SizedBox(
              child: Expanded(
                    child: SizedBox(
                      height: double.maxFinite,
                      child: FutureBuilder<ShopSearch>(
                          future: listShopsByFoodType,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              // Return a loading indicator or placeholder widget
                              return  ListView.builder(
                                itemCount: 8,
                                itemBuilder: (BuildContext context, int index) {
                                  return  const CardLoading(
                                    height: 100,
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    margin: EdgeInsets.only(bottom: 10),
                                  );
                                },
                              );
                            } else if (snapshot.hasError) {
                              // Handle the error state, you can display an error message

                              return  const NotFound404Error(text: 'No data available', image: 'assets/svg/404_error.svg',);

                            } else if (!snapshot.hasData) {
                              // Handle the case where the future completed with no data
                              return const Text('No data available');
                            } else {
                              return ListView.builder(
                                physics: const PageScrollPhysics(),
                                itemCount:
                                    snapshot.data!.returnData!.data!.length,
                                itemBuilder: (_, index) {

                                  var product =
                                      snapshot.data!.returnData!.data!.toList();

                                  return ShopRowItemWidget(product[index]);

                                },
                              );
                            }
                          }),
                    ),
                  ),

            ),
          ),
        ),
      ])),
    );
  }

  restaurantSkeleton() {

    return Shimmer.fromColors(
      baseColor: myCustomColor,
      highlightColor: myCustomGreyColor,
      child: Container(
        margin: const EdgeInsets.all(5),
        child: PhysicalModel(
          color: Colors.white,
          elevation: 5,
          shadowColor: Colors.white,
          borderRadius: BorderRadius.circular(10),
          child: InkWell(
            onTap: () {
              // تنفيذ عمل معين عند النقر
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 60,
                            width: 70,
                            color: Colors.white, // لون الصورة المؤقتة
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

}
