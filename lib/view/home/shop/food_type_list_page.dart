import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food/controller/shop_controller.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/model/address/UserAddress.dart';
import 'package:food/model/shop/ProductsByFoodType.dart';
import 'package:food/widgets/head_widget.dart';
import 'package:food/widgets/shop/product_info_widget.dart';
import 'package:food/widgets/shop/shop_info_widget.dart';
import 'package:get/get.dart';

import 'package:intl/intl.dart';

class FoodTypeListPage extends StatefulWidget {
  const FoodTypeListPage(
      {Key? key, required this.id, required this.foodTypeName})
      : super(key: key);

  final String id;
  final String foodTypeName;

  @override
  State<FoodTypeListPage> createState() => _FoodTypeListPageState();
}

class _FoodTypeListPageState extends State<FoodTypeListPage> {
  final _userAddressController = Get.put(UserAddressController());
  final _shopController = Get.put(ShopController());

  late Future<String> userAddress;
  late Future<UserAddress> listOfUserAddress;
  late Future<ProductsByFoodType> listProductsByFoodType;

  NumberFormat formatter = NumberFormat("0.00");

  @override
  void initState() {
    super.initState();
    userAddress = _userAddressController.getLocationAndAddress();
    listOfUserAddress = _userAddressController.getUserAddress();

    listProductsByFoodType = _shopController.getProductsByFoodType(
        widget.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(children: [
        HeadWidget(
            text: widget.foodTypeName,
            userAddressController: _userAddressController,
            listOfUserAddress: listOfUserAddress,
            userAddress: userAddress),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "All ${widget.foodTypeName}",
                    style: const TextStyle(fontSize: 16),
                  ),
                  Expanded(
                    child: SizedBox(
                      height: double.maxFinite,
                      child: FutureBuilder<ProductsByFoodType>(
                          future: listProductsByFoodType,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              // Return a loading indicator or placeholder widget
                              return const Center(
                                  child: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: CircularProgressIndicator()));
                            } else if (snapshot.hasError) {
                              // Handle the error state, you can display an error message
                              return Text('Error: ${snapshot.error}');
                            } else if (!snapshot.hasData) {
                              // Handle the case where the future completed with no data
                              return const Text('No data available');
                            } else {
                              return ListView.builder(
                                physics: const PageScrollPhysics(),
                                itemCount: snapshot.data!.returnData!.length,
                                itemBuilder: (_, index) {
                                  var shop = snapshot.data!.returnData![index];
                                  var productDetailsList = shop.productDetails!
                                      .where((e) => e.isActive == true)
                                      .toList();

                                  return Container(
                                    margin: const EdgeInsets.all(5),
                                    child: PhysicalModel(
                                      color: Colors.white,
                                      elevation: 5,
                                      shadowColor: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      flex: 2,
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8.0),
                                                        child: Expanded(
                                                          child: Container(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child:
                                                                CachedNetworkImage(
                                                              height: 60,
                                                              width: 60,
                                                              fit: BoxFit
                                                                  .fitHeight,
                                                              imageUrl: shop
                                                                  .shopAvatar
                                                                  .toString(),
                                                              progressIndicatorBuilder:
                                                                  (context, url,
                                                                          downloadProgress) =>
                                                                      Center(
                                                                child: SizedBox(
                                                                  height: 15,
                                                                  width: 15,
                                                                  child: CircularProgressIndicator(
                                                                      value: downloadProgress
                                                                          .progress),
                                                                ),
                                                              ),
                                                              errorWidget: (context,
                                                                      url,
                                                                      error) =>
                                                                  const Icon(Icons
                                                                      .error),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 4,
                                                      child: ShopInfoWidget(
                                                          shop: shop),
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(
                                                    height: 18,
                                                    child: Divider(
                                                      color: Colors.black,
                                                    )),
                                                SizedBox(
                                                  height: 150,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: buildShopProductList(
                                                        productDetailsList),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                          }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ])),
    );
  }


  //////*****

  Widget buildShopProductList(productDetailsList) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      physics: const PageScrollPhysics(),
      itemCount:
          productDetailsList.where((e) => e.isActive == true).toList().length,
      itemBuilder: (_, index) {
        var productDetails = productDetailsList[index];
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              ProductInfoWidget(productDetails: productDetails),
              const SizedBox(child: VerticalDivider(color: Colors.green)),
            ],
          ),
        );
      },
    );
  }
}
