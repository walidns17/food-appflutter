import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/view/home/tab/home_page.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final List<Widget> _tabs = [
    const HomePage(),
    Container(),
    Container(),
    Container(),

    // const OrderTab(),
    // const MenuTab(),
    // const MoreTab()
  ];

  final _userAddressController = Get.put(UserAddressController());

  @override
  void initState() {
    super.initState();


    _userAddressController.determinePosition();

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        child: CupertinoTabScaffold(
            tabBar: CupertinoTabBar(
              height: 60,
              backgroundColor: Colors.white,
              //change here
              activeColor: Colors.blueAccent,
              // here
              inactiveColor: Colors.black,
              // here too
              items: const [
                BottomNavigationBarItem(
                    icon: Image(
                        image: AssetImage("assets/icons/home.png"), height: 24),
                    label: 'Home'),
                BottomNavigationBarItem(
                    icon: Image(
                        image: AssetImage("assets/icons/favorites.png"),
                        height: 24),
                    label: 'Favorite'),
                BottomNavigationBarItem(
                    icon: Image(
                        image: AssetImage("assets/icons/orders.png"),
                        height: 24),
                    label: 'Orders'),
                BottomNavigationBarItem(
                    icon: Image(
                        image: AssetImage("assets/icons/user.png"), height: 24),
                    label: 'Profile'),
              ],
            ),
            tabBuilder: (BuildContext context, index) {
              return _tabs[index];
            }),
      ),
    );
  }
}
