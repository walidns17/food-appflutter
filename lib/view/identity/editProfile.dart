import 'package:flutter/material.dart';
import '../../utils/global.colors.dart';
import '../../widgets/button.global.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key, required this.fullname, required this.email, required this.imagePath}) : super(key: key);

  final String fullname;
  final String email;
  final String imagePath;


  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  bool showPassword = false;

  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: ListView(
          children: [
            Container(
              color: Colors.white30,
              child: const Padding(
                padding: EdgeInsets.all(18.0),
                child: Text(
                  "تحديث الملف الشخصي",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 14, 0),
              child: Text(
                'اكمل التسجيل',
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w200,
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(14, 0, 14, 0),
              child: Text(
                'التفاصيل المطلوبة',
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Center(
              child: Stack(
                children: [
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 4,
                            color: Theme.of(context).scaffoldBackgroundColor),
                        boxShadow: [
                          BoxShadow(
                              spreadRadius: 2,
                              blurRadius: 10,
                              color: Colors.black.withOpacity(0.1),
                              offset: const Offset(0, 10))
                        ],
                        shape: BoxShape.circle,
                        image: const DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('assets/images/logo_app.png'))),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 4,
                            color: Theme.of(context).scaffoldBackgroundColor,
                          ),
                          color: Colors.green,
                        ),
                        child: const Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                      )),
                ],
              ),
            ),
            const SizedBox(
              height: 35,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(children: [

                buildTextField("اسم المستخدم", "assets/icons/user.png",
                    usernameController,TextInputType.name),

                buildTextField("البريد الالكتروني", "assets/icons/email.png",
                    emailController,TextInputType.emailAddress),
                const SizedBox(
                  height: 30,
                ),
                ButtonGlobal(
                  onPress: () => _onPressUpdateProfile(), text: '' ,
                ),
              ]),
            ),
            const SizedBox(
              height: 35,
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [],
            )
          ],
        ),
      ),
    );
  }

  Widget buildTextField(
      String labelText, String image, TextEditingController controller,TextInputType textInputType) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Container(
        padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: GlobalColors.buttonColors)),
        child: TextField(
          controller: controller,
          keyboardType: textInputType,

          decoration: InputDecoration(

            prefixIcon: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Image.asset(
                image,
                width: 3,
                height: 3,
              ),
            ),
            border: InputBorder.none,

            labelText: labelText,
          ),
        ),
      ),
    );
  }



  void _onPressUpdateProfile() {
 //   showDialogProgress(context);
    if (!(usernameController.text.isNotEmpty)) {
      _scaffoldMessenger();
    } else {
    //  customerIdentityServices.PutUpdateProfile(emailController.text,usernameController.text);
    }
  }

  _scaffoldMessenger() {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('يرجى إدخال الحقول المطلوبة'),
      backgroundColor: Colors.indigo,
    ));
    Navigator.of(context).pop();
  }


}
