import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

import '../../controller/login_controller.dart';
import '../../widgets/button.global.dart';
import 'package:get/get.dart';

class OTPVerify extends StatefulWidget {
  const OTPVerify({Key? key, required this.phoneNumber}) : super(key: key);

  final String phoneNumber;

  @override
  State<OTPVerify> createState() => _OTPVerifyState();
}

class _OTPVerifyState extends State<OTPVerify> {
  final _loginController = Get.put(LoginController());

  TextEditingController countryController = TextEditingController();

  final _otpNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(left: 25, right: 25),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/logo.png',
                width: 150,
                height: 150,
              ),
              const SizedBox(
                height: 25,
              ),
              const Text(
                "التحقق من الهاتف",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "نحتاج إلى تسجيل هاتفك قبل البدا",
                style: TextStyle(
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 30,
              ),
              Pinput(
                forceErrorState: true,
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                controller: _otpNumberController,
                validator: (pin) {
                  _otpNumberController.text == pin;
                  return null;
                  // if (pin == '1234') return null;
                  // return 'رمز التحقيق غير صحيح';
                },
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ButtonGlobal(
                  onPress: _onPressCheckPhone,
                  text: 'Verify OTP',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onPressCheckPhone() {
    if (!(_otpNumberController.text.length > 3)) {
      _scaffoldMessenger();
    } else {
      _loginController.getOTP(
          _otpNumberController.text, widget.phoneNumber, 0, 0, context);
    }
  }

  _scaffoldMessenger() {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('يرجى إدخال رقم  مكون من 4 ارقام'),
      backgroundColor: Colors.indigo,
    ));
    Navigator.of(context).pop();
  }
}
