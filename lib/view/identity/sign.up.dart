import 'package:flutter/material.dart';
import 'package:food/controller/login_controller.dart';
import 'package:get/get.dart';
import '../../widgets/button.global.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final TextEditingController _phoneNumberController = TextEditingController();

  final _loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(left: 15, right: 15, top: 50),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: 55,
                height: 55,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Center(
                  child: Icon(Icons.close_outlined),
                ),
              ),
              const SizedBox(height: 20),
              const Row(
                children: [
                  Text(
                    "Welcome ",
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 22,
                    ),
                  ),
                  Text(
                    "to our",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              const Row(
                children: [
                  Text(
                    "faster delivery ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 100),

              // Phone Verification
              Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.fromLTRB(14, 0, 0, 0),
                child: const Text(
                  "Phone Verification",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 4.17,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Image.asset(
                            'assets/icons/algeria.png',
                            width: 30,
                            height: 30,
                          ),
                        ),
                        const Text("+213"),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Container(
                    height: 55,
                    width: MediaQuery.of(context).size.width / 1.77,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      controller: _phoneNumberController,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        hintText: '0000000000',
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                        ),
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ButtonGlobal(
                  onPress: _onPressCheckPhone,
                  text: "Continue",
                ),
              ),
              const SizedBox(height: 20),
              const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "If you sign up, Terms & Conditions and Privacy policy apply.",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 10,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onPressCheckPhone() {
    if (!(_phoneNumberController.text.length > 8)) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('يرجى إدخال رقم الجوال مكون من 9 ارقام'),
          backgroundColor: Colors.indigo,
        ),
      );
    } else {
      _loginController.getLogin("966${_phoneNumberController.text}", context);
    }
  }

  Future<void> _v(phoneNumber) async {

    print("1997$phoneNumber");

  }
}
