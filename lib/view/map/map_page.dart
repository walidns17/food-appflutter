import 'dart:async';
import 'package:flutter/services.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/widgets/button.global.dart';
import 'package:food/widgets/text_form_field.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:map_picker/map_picker.dart';
import 'package:geocoding/geocoding.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  final _controller = Completer<GoogleMapController>();
  MapPickerController  mapPickerController = MapPickerController();
  final _userAddressController = Get.put(UserAddressController());


  late Position? currentPosition; // تعيينه إلى null في البداية

  late CameraPosition cameraPosition =  CameraPosition(
    target: LatLng(
      currentPosition?.latitude ?? 0, // استخدام الموقع الحالي إذا كان متاحًا، وإلا استخدام الإعدادات الافتراضية
      currentPosition?.longitude ?? 0,
    ),
    zoom: 15,
  );




  var textControllerSubLocality = TextEditingController();
  var textControllerStreet = TextEditingController();

  final _controllerAddress = TextEditingController();
  final _controllerStreet = TextEditingController();
  final _controllerDescription = TextEditingController();
  final _controllerLatitude = TextEditingController();
  final _controllerLongitude = TextEditingController();
  final _controllerZipCode = TextEditingController();


  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  void _getCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );

      setState(() {
        currentPosition = position;
      });
    } catch (e) {
      print("Error getting location: $e");
    }
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          MapPicker(
            // pass icon widget
            iconWidget: SvgPicture.asset(
              "assets/svg/map_pin.svg",
              height: 60,
            ),
            //add map picker controller
            mapPickerController: mapPickerController,
            child: GoogleMap(
              myLocationEnabled: true,
              zoomControlsEnabled: false,
              // hide location button
              myLocationButtonEnabled: false,
              mapType: MapType.normal,
              //  camera position
              initialCameraPosition: CameraPosition(
                target: LatLng(
                  currentPosition?.latitude ?? 0, // استخدام الموقع الحالي إذا كان متاحًا، وإلا استخدام الإعدادات الافتراضية
                  currentPosition?.longitude ?? 0,
                ),
                zoom: 15,
              ),

              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              onCameraMoveStarted: () {
                // notify map is moving
                mapPickerController.mapMoving!();
              },
              onCameraMove: (cameraPosition) {
                this.cameraPosition = cameraPosition;
              },
              onCameraIdle: () async {
                // notify map stopped moving
                mapPickerController.mapFinishedMoving!();

                //get address name from camera position
                List<Placemark> placemarks = await placemarkFromCoordinates(
                  cameraPosition.target.latitude,
                  cameraPosition.target.longitude,
                );

                //thoroughfare الشارع
                // update the ui with the address
                textControllerSubLocality.text =
                '${placemarks.first.subLocality}';
                textControllerStreet.text = '${placemarks.first.street}';
                _controllerStreet.text = '${placemarks.first.subLocality}';
                _controllerDescription.text = '${placemarks.first.street}';

                _controllerLatitude.text = cameraPosition.target.latitude.toString();
                _controllerLongitude.text = cameraPosition.target.longitude.toString();
                _controllerZipCode.text = '${placemarks.first.postalCode}';

              },
            ),
          ),
          Positioned(
            top: MediaQuery
                .of(context)
                .viewPadding
                .top + 0,
            width: MediaQuery
                .of(context)
                .size
                .width - 0,
            child: _buildLocationInfo(),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.maxFinite,
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    TextFormField(
                      maxLines: 1,
                      textAlign: TextAlign.end,
                      readOnly: true,
                      enabled: false,
                      decoration: const InputDecoration(
                          contentPadding:
                          EdgeInsets.only(right: 18, left: 18, top: 18),
                          // Add padding for spacing
                          suffixIcon: Icon(Icons.location_pin),
                          // Icon on the right
                          border: InputBorder.none),
                      controller: textControllerSubLocality,
                    ),
                    TextFormField(
                      maxLines: 2,
                      textAlign: TextAlign.end,
                      readOnly: true,
                      decoration: const InputDecoration(
                          contentPadding: EdgeInsets.only(right: 30),
                          // Add padding for spacing

                          border: InputBorder.none),
                      controller: textControllerStreet,
                    ),
                    const Divider(),

                    SizedBox(
                      child: TextFormFieldWidget(maxLines: 1,
                          hintText: "Home",
                          controller: _controllerAddress),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: SizedBox(
                        height: 50,
                        width: double.maxFinite,
                        child: ButtonGlobal(
                            onPress: () {

                              _userAddressController.postUserAddress(
                                  _controllerAddress.text, _controllerStreet.text, _controllerDescription.text,
                                  _controllerLatitude.text, _controllerLongitude.text, _controllerZipCode.text,context);

                            }, text: "Confirm Location"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _buildLocationInfo() {
    return Container(
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: Stack(
                alignment: Alignment.topLeft,
                children: <Widget>[
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Expanded(
            flex: 2,
            child: Text("Select your location", style: TextStyle(fontSize: 18)),
          ),
        ],
      ),
    );
  }


}
