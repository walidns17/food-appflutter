import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:food/view/home/pages/home.dart';
import 'package:food/view/identity/sign.up.dart';
import 'package:get/get.dart';


class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  static const storage = FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {

    Timer(const Duration(seconds: 3), () {
      _onCheckToToken();
    });
    return Scaffold(
      backgroundColor: Colors.white,
      body:  Center(

          child: Image.asset('assets/images/logo.png',width: 100,height: 100,)
      ),
    );
  }

  Future<void> _onCheckToToken() async {

    String? token = await storage.read(key: "token");


    if(!token.isNull){
      Get.offAll((const Home()));
    }else{
      Get.offAll((const SignUp()));

    }

  }

}