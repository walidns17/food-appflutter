import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:food/components/cart_provider.dart';
import 'package:food/model/cart_Item.dart';
import 'package:food/view/screen/cart/components/check_out_card.dart';
import 'package:provider/provider.dart';

import 'components/cart_card.dart';



class CartScreen extends StatelessWidget {
  static String routeName = "/cart";

  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {

    final cartProvider = Provider.of<CartProvider>(context, listen: false);
    List<Cart> cartItems = cartProvider.cartItems;

    return Scaffold(
      appBar: buildAppBar(context,cartProvider),
      body: body(cartItems,cartProvider),
      bottomNavigationBar: CheckoutCard(),
    );
  }

  AppBar buildAppBar(BuildContext context,cartProvider) {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.black),
      title: Container(
        width: double.maxFinite,
        alignment: Alignment.center,
        child: Column(
          children: [
            const Text(
              "Your Cart",
              style: TextStyle(color: Colors.black),
            ),
            Consumer<CartProvider>(
              builder: (context, cartProvider, child) {
               return Text(
                  "${cartProvider.cartItems.length} items",
                  style: Theme.of(context).textTheme.caption,
                );
              },
            ),

          ],
        ),
      ),

    );
  }

  body(cartItems,cartProvider){

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: ListView.builder(
        itemCount: cartItems.length,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Dismissible(
            key: Key(cartItems[index].toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) {
              // إزالة العنصر من السلة وتحديث الواجهة

                cartProvider.removeFromCart(cartItems[index]);

            },
            background: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: const Color(0xFFFFE6E6),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: [
                  const Spacer(),
                  SvgPicture.asset("assets/svg/Trash.svg"),
                ],
              ),
            ),
            child: CartCard(carts: cartItems[index]),
          ),
        ),
      ),
    );

  }


}