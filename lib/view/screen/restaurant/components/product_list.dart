import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food/model/shop/ShopMenue.dart';
import 'package:food/view/screen/404_error/not_found_error.dart';
import 'package:food/view/screen/restaurant/components/product_row.dart';
import 'package:redacted/redacted.dart';

class ProductList extends StatelessWidget {
  late final int selected;
  late final Function callback;
  late Future<ShopMenue> listShopMenue;
  final PageController pageController;

  ProductList(
      this.selected, this.callback, this.pageController, this.listShopMenue,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: FutureBuilder<ShopMenue>(
          future: listShopMenue,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              // Return a loading indicator or placeholder widget
              return ListView.builder(
                itemCount: 8,
                itemBuilder: (BuildContext context, int index) {
                  return  Expanded(
                    child: Container(
                      height: 110,
                      width: double.maxFinite,
                      color: Colors.white12,
                      margin: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0), //add border radius
                              child: Container(
                                height: 80,
                                width: 80,
                                color: Colors.black26,
                              ),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  padding: const EdgeInsets.only(
                                    top: 20,
                                    left: 10,
                                    right: 10,
                                  ),
                                  child: const Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              "",
                                            ),
                                          ),

                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),

                                    ],
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ).redacted(context: context, redact: true),
                  );
                }

              );
            } else if (snapshot.hasError) {
              // Handle the error state, you can display an error message
              return Text('Error: ${snapshot.error}');
            } else if (!snapshot.hasData) {
              // Handle the case where the future completed with no data
              return const Text('No data available');
            } else {

              var category = snapshot.data!.returnData!.toList();

              if (category.isEmpty) {
                // إذا كان فارغاً
                return  const NotFound404Error(text: 'You have no restaurant items.', image: 'assets/svg/shopping.svg',);

              } else {
                return PageView(
                  controller: pageController,
                  onPageChanged: (index) => callback(index),
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const PageScrollPhysics(),
                      itemCount: (category.isNotEmpty && selected >= 0 &&
                          selected < category.length)
                          ? (category[selected].productDetails?.length ?? 0)
                          : 0,
                      itemBuilder: (_, index) {
                        if (category.isNotEmpty &&
                            selected >= 0 &&
                            selected < category.length &&
                            category[selected].productDetails != null &&
                            index >= 0 &&
                            index < category[selected].productDetails!
                                .length) {
                          var product = category[selected]
                              .productDetails![index];

                          return ProductRow(product);
                        }
                      },
                    ),
                  ],
                );
              }

            }
          }),
    );
  }
}
