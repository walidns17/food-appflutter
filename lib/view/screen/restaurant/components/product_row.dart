import 'dart:async';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_glow/flutter_glow.dart';
import 'package:food/components/cart_provider.dart';
import 'package:food/components/toast.dart';
import 'package:food/constans/colors.dart';
import 'package:food/controller/shop_controller.dart';
import 'package:food/model/cart_Item.dart';
import 'package:food/model/shop/ProductModel.dart';
import 'package:food/model/shop/ShopMenue.dart';
import 'package:food/widgets/button.global.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class ProductRow extends StatefulWidget {
  final ProductDetails productDetails;

  ///ShopMenue

  const ProductRow(this.productDetails, {super.key});

  @override
  State<ProductRow> createState() => _FoodItem();
}

class _FoodItem extends State<ProductRow> {
  final _shopController = Get.put(ShopController());

  late Future<ProductModel> product;
  late int? productDetailsSizesId = 0;
  AdditionalComponentManager additionalCompnentManager =
      AdditionalComponentManager();
  SubAdditionalComponentManager subAdditionalCompnentManager =
      SubAdditionalComponentManager();
  int quantityCount = 1;
  double priceCount = 0;

  double totalPrice = 0;

  List<AdditionalComponentTitles>? additionalComponentTitle;
  List<SubAdditionalComponentTitles>? subAdditionalComponentTitle;

  void decrementQuantity() {
    setState(() {
      if (quantityCount > 1) {
        quantityCount--;
        sumPriceTotal();
      }
    });
  }

  void incrementQuantity() {
    setState(() {
      quantityCount++;
      sumPriceTotal();
    });
  }

  void sumPriceTotal() {
    double totalAdditionalComponentPrice = additionalCompnentManager.sumPrice();
    double totalSubAdditionalComponentPrice =
        subAdditionalCompnentManager.sumPrice();
    totalPrice = (totalAdditionalComponentPrice +
            totalSubAdditionalComponentPrice +
            priceCount) *
        quantityCount;
  }

  @override
  void initState() {
    super.initState();
    product = _shopController.getProductItem(widget.productDetails.id);
    priceCount = widget.productDetails.price!;
    sumPriceTotal();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          showDialog(context);
        });
      },
      child: Container(
        height: 110,
        width: double.maxFinite,
        margin: const EdgeInsets.all(5),
        child: PhysicalModel(
          color: Colors.white,
          elevation: 5,
          shadowColor: Colors.white,
          borderRadius: BorderRadius.circular(10),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0), //add border radius
                  child: CachedNetworkImage(
                    height: 80,
                    width: 80,
                    fit: BoxFit.fill,
                    imageUrl: widget.productDetails.coverImage.toString(),
                    progressIndicatorBuilder: (context, url, downloadProgress) {
                      return Center(
                        child: SizedBox(
                          height: 15,
                          width: 15,
                          child: CircularProgressIndicator(
                              value: downloadProgress.progress),
                        ),
                      );
                    },
                    errorWidget: (context, url, error) {
                      return const Icon(Icons.error);
                    },
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 10,
                  right: 10,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            widget.productDetails.title.toString(),
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              height: 1.5,
                            ),
                          ),
                        ),
                        const Icon(
                          Icons.arrow_forward_ios_outlined,
                          size: 15,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Text(
                          widget.productDetails.price.toString(),
                          style: const TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const Text(
                          " SR",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )),
            ],
          ),
        ),
      ),
    );
  }

  showDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
          topRight: Radius.circular(25.0),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return DraggableScrollableSheet(
              expand: false,
              initialChildSize: 0.7,
              maxChildSize: 0.9,
              minChildSize: 0.32,
              builder: (context, scrollController) {
                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  clipBehavior: Clip.hardEdge,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(20),
                        topRight: const Radius.circular(20)),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 5,
                              width: 35,
                              color: Colors.black12,
                            )
                          ],
                        ),
                      ),
                      scroll(scrollController, setState),
                      buttonAddCart(setState),
                      const SizedBox(height: 20)
                    ],
                  ),
                );
              },
            );
          },
        );
      },
    ).whenComplete(() {
      // عند إغلاق القاعدة السفلية، قم بإعادة تحديث الواجهة وحذف البيانات
      setState(() {
        Navigator.of(context);
      });
    });
  }

  buttonArrow(StateSetter setState) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 35),
          child: Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.all(3),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                width: double.maxFinite,
                height: 200,
                alignment: Alignment.topCenter,
                imageUrl: widget.productDetails.coverImage.toString(),
                progressIndicatorBuilder: (context, url, downloadProgress) {
                  return Center(
                    child: SizedBox(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress,
                      ),
                    ),
                  );
                },
                errorWidget: (context, url, error) {
                  return const Icon(Icons.error);
                },
              ),
            ),
          ),
        ),
        Positioned(
          top: 170,
          right: 20,
          child: Container(
            height: 50,
            margin: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: const Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.all(5),
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: IconButton(
                          onPressed: () {
                            setState(() {
                              decrementQuantity();
                            });
                          },
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 120,
                        child: Center(
                          child: Text(
                            quantityCount.toString(),
                            style: const TextStyle(
                                color: mainText,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(5),
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: IconButton(
                            onPressed: () {
                              setState(() {
                                incrementQuantity();
                              });
                            },
                            icon: const Icon(
                              Icons.add,
                              color: primary,
                            )),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  scroll(scrollController, StateSetter setState) {
    return Expanded(
      child: SingleChildScrollView(
        controller: scrollController,
        physics: const BouncingScrollPhysics(), // تجربة مع إعدادات الـ physics
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buttonArrow(setState),
            const SizedBox(
              height: 5,
            ),
            Text(
              widget.productDetails.title.toString(),
              style: Theme.of(context).textTheme.headline6,
            ),

            const SizedBox(
              height: 10,
            ),
            Text(
              widget.productDetails.note.toString(),
              style: const TextStyle(fontSize: 14),
            ),
            const SizedBox(
              height: 50,
            ),

            // List productDetailsSizes
            buildProductDetails(setState),
          ],
        ),
      ),
    );
  }

  buttonAddCart(StateSetter setState) {
    return Positioned(
      bottom: 0, // Adjust the position as needed
      left: 0,
      right: 0,
      child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
          child: Center(
            child: ButtonGlobal(
                text: "ADD",
                onPress: () {
                  setState(() {
                    addToCart(widget.productDetails, totalPrice, quantityCount,
                        setState);
                  });
                }),
          )),
    );
  }

  buildProductDetails(StateSetter setState) {
    return FutureBuilder<ProductModel>(
      future: product,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else if (!snapshot.hasData) {
          return const Text('No data available');
        } else {
          var pDetailsSizes =
              snapshot.data!.returnData!.productDetailsSizes!.toList();
          var additionalComponentTitles =
              snapshot.data!.returnData!.additionalComponentTitles!.toList();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _productDetailsSizes(pDetailsSizes, setState),
              const SizedBox(height: 40, child: Divider()),
              _productOptions(additionalComponentTitles, setState),
            ],
          );
        }
      },
    );
  }

  textItem(
      {required String title, required String value, required String unit}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            title,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            softWrap: false,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.black,
            ),
          ),
        ),
        Row(
          children: [
            Text(
              value,
              style: const TextStyle(
                fontSize: 12,
                color: Colors.black,
              ),
            ),
            Text(
              unit,
              style: const TextStyle(
                fontSize: 12,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ],
    );
  }

  _productDetailsSizes(
      List<ProductDetailsSizes>? pDetailsSizes, StateSetter setState) {
    bool isSelected = false;

    if (pDetailsSizes!.isNotEmpty) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (pDetailsSizes.any((item) => item.itemSizeNameEn != 'Deliverect'))
            Container(
              margin: const EdgeInsets.fromLTRB(18, 0, 18, 0),
              child: const Text(
                "Size",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: pDetailsSizes.length,
            itemBuilder: (_, index) {
              if (pDetailsSizes.isNotEmpty &&
                  pDetailsSizes[index].itemSizeNameEn == 'Deliverect') {
                productDetailsSizesId = pDetailsSizes[index].id;
                priceCount = pDetailsSizes[index].price!;
                sumPriceTotal();
              } else {
                return Column(
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          productDetailsSizesId =
                              !isSelected ? pDetailsSizes[index].id : null;

                          priceCount = pDetailsSizes[index].price!;

                          sumPriceTotal();
                        });
                      },
                      child: ClipRRect(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Center(
                                child: ListTile(
                                  title: textItem(
                                      title: pDetailsSizes[index]
                                          .itemSizeNameEn
                                          .toString(),
                                      value:
                                          "(+ ${pDetailsSizes[index].price} SAR )",
                                      unit: ""),
                                  trailing: productDetailsSizesId ==
                                          pDetailsSizes[index].id
                                      ? const Icon(
                                          Icons
                                              .radio_button_checked, // رمز الاختيار
                                          color: Colors.green, // لون الاختيار
                                        )
                                      : const Icon(Icons.radio_button_off),
                                ),
                              ),
                            ),
                            //const Icon(Icons.arrow_forward_ios, color: Colors.blue),
                          ],
                        ),
                      ),
                    ),
                    const Divider(color: mainText),
                  ],
                );
              }
            },
          )
        ],
      );
    } else {
      return Container(); // عندما تكون القائمة فارغة، لا تعرض شيء.
    }
  }

  ///
  _productOptions(List<AdditionalComponentTitles>? additionalComponentTitles,
      StateSetter setState) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: additionalComponentTitles!.length,
      itemBuilder: (_, index) {
        String isRequired = additionalComponentTitles[index].isRequired!
            ? "Required"
            : "optional";

        additionalComponentTitle = additionalComponentTitles;

        return Padding(
            padding: const EdgeInsets.only(bottom: 5, top: 5),
            child: Column(
              children: [
                InkWell(
                  onTap: () {},
                  child: ClipRRect(
                    child: Container(
                      margin: const EdgeInsets.all(0),
                      decoration: const BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4.0),
                          bottomLeft: Radius.circular(4.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  additionalComponentTitles[index]
                                      .title
                                      .toString(),
                                  style: const TextStyle(
                                      color: mainText, fontSize: 16),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text(
                                    "Choose ${additionalComponentTitles[index].numberOfSelect}",
                                    style: const TextStyle(
                                        color: SecondaryText, fontSize: 14),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 10, right: 10, top: 5, bottom: 5),
                                    decoration: const BoxDecoration(
                                      color: outline,
                                      shape: BoxShape.rectangle,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                    ),
                                    child: Text(
                                      isRequired,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                          color: mainText, fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            _additionalComponents(
                                additionalComponentTitles[index]
                                    .additionalComponents,
                                additionalComponentTitles[index].numberOfSelect,
                                setState),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                const Divider(color: mainText),
              ],
            ));
      },
    );
  }

  Widget _additionalComponents(List<AdditionalComponents>? additionalComponents,
      numberOfSelect, StateSetter setState) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: additionalComponents!.length,
      itemBuilder: (_, index) {
        final AdditionalComponents additionalComponent =
            additionalComponents[index];

        return buildAdditionalComponentItem(
            additionalComponent, numberOfSelect, setState);
      },
    );
  }

  Widget buildAdditionalComponentItem(AdditionalComponents additionalComponent,
      int numberOfSelect, StateSetter setState) {
    String price = (additionalComponent.price! > 0)
        ? '${additionalComponent.price!} SAR'
        : '';

    final bool isSelected = additionalCompnentManager
        .isAdditionalComponentExist(additionalComponent.id!.toInt());

    return Padding(
      padding: const EdgeInsets.only(bottom: 5, top: 5),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                if (additionalCompnentManager
                        .isSum(additionalComponent.additionalComponentTitleId) <
                    numberOfSelect) {
                  additionalCompnentManager.addAdditionalComponent(
                      additionalComponent.price!,
                      1,
                      additionalComponent.id!,
                      additionalComponent.additionalComponentTitleId!);
                } else {
                  additionalCompnentManager
                      .removeAdditionalComponent(additionalComponent.id!);
                }
                sumPriceTotal();
              });
            },
            child: ClipRRect(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Center(
                      child: ListTile(
                        title: textItem(
                            title: additionalComponent.componentName.toString(),
                            value: price,
                            unit: ""),
                        trailing: isSelected
                            ? const Icon(
                                Icons.check_box_rounded, // رمز الاختيار
                                color: Colors.green, // لون الاختيار
                              )
                            : const Icon(Icons.check_box_outline_blank),
                      ),
                    ),
                  ),
                  //const Icon(Icons.arrow_forward_ios, color: Colors.blue),
                ],
              ),
            ),
          ),
          const Divider(color: mainText),
        ],
      ),

      //
      // Column(
      //   children: [
      //     Center(
      //       child: InkWell(
      //         onTap: () {
      //           setState(() {
      //             if (additionalCompnentManager
      //                 .isSum(additionalComponent.additionalComponentTitleId) <
      //                 numberOfSelect) {
      //               additionalCompnentManager.addAdditionalComponent(
      //                   additionalComponent.price!,
      //                   1,
      //                   additionalComponent.id!,
      //                   additionalComponent.additionalComponentTitleId!);
      //
      //               // تعديل قيمة isContained بعد إضافة العنصر
      //               isContained = true;
      //             } else {
      //               additionalCompnentManager
      //                   .removeAdditionalComponent(additionalComponent.id!);
      //
      //               // تعديل قيمة isContained بعد إزالة العنصر
      //               isContained = false;
      //             }
      //             sumPriceTotal();
      //           });
      //         },
      //         child: ClipRRect(
      //           child: Row(
      //             children: <Widget>[
      //               Container(
      //                 margin: const EdgeInsets.all(0),
      //                 decoration: const BoxDecoration(
      //                   shape: BoxShape.rectangle,
      //                   borderRadius: BorderRadius.only(
      //                     topLeft: Radius.circular(4.0),
      //                     bottomLeft: Radius.circular(4.0),
      //                   ),
      //                 ),
      //                 width: 20,
      //                 height: 73,
      //               ),
      //               const SizedBox(width: 10),
      //               Expanded(
      //                 child: Center(
      //                   child: ListTile(
      //                     title: textItem(
      //                       title: additionalComponent.componentName.toString(),
      //                       value: price,
      //                       unit: "",
      //                     ),
      //                     onTap: () {
      //                       setState(() {
      //                         if (additionalComponent
      //                             .subAdditionalComponentTitles!.isNotEmpty) {
      //                           _showDialogSub(
      //                               additionalComponent
      //                                   .subAdditionalComponentTitles,
      //                               context,
      //                               setState);
      //                         }
      //                       });
      //                     },
      //                     trailing: isContained
      //                         ? const Icon(
      //                       Icons.radio_button_checked, // Checked icon
      //                       color: Colors.green, // Checked color
      //                     )
      //                         : const Icon(
      //                         Icons.radio_button_off), // Unchecked icon
      //                   ),
      //                 ),
      //               ),
      //             ],
      //           ),
      //         ),
      //       ),
      //     ),
      //     const Divider(color: mainText),
      //   ],
      // ),
    );
  }

  void _showDialogSub(
      List<SubAdditionalComponentTitles>? subAdditionalComponentTitles,
      BuildContext context,
      StateSetter setState) {
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            // <-- SEE HERE
            topLeft: Radius.circular(25.0),
          ),
        ),
        builder: (context) {
          return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: subAdditionalComponentTitles!.length,
            itemBuilder: (_, index) {
              String isRequired =
                  subAdditionalComponentTitles[index].isRequired!
                      ? "optional"
                      : "Required";

              return Padding(
                  padding: const EdgeInsets.only(bottom: 5, top: 5),
                  child: PhysicalModel(
                      color: Colors.white,
                      elevation: 5,
                      shadowColor: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      child: InkWell(
                        onTap: () {},
                        child: ClipRRect(
                          child: Container(
                            margin: const EdgeInsets.all(0),
                            decoration: const BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(4.0),
                                bottomLeft: Radius.circular(4.0),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        subAdditionalComponentTitles[index]
                                            .title
                                            .toString(),
                                        style: const TextStyle(
                                            color: mainText, fontSize: 16),
                                      ),
                                      Container(
                                        width: 100,
                                        padding: const EdgeInsets.only(
                                            left: 10,
                                            right: 10,
                                            top: 5,
                                            bottom: 5),
                                        decoration: const BoxDecoration(
                                          color: mainText,
                                          shape: BoxShape.rectangle,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                        ),
                                        child: Text(
                                          isRequired,
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                              color: form, fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Choose until ${subAdditionalComponentTitles[index].numberOfSelect}",
                                      style: const TextStyle(
                                          color: mainText, fontSize: 14),
                                    ),
                                  ),
                                  _subAdditionalComponent(
                                      subAdditionalComponentTitles[index]
                                          .subAdditionalComponents,
                                      subAdditionalComponentTitles[index]
                                          .numberOfSelect,
                                      setState),
                                ],
                              ),
                            ),
                          ),
                        ),
                      )));
            },
          );
        });
  }

  Widget _subAdditionalComponent(
      List<SubAdditionalComponents>? subAdditionalComponents,
      numberOfSelect,
      StateSetter setState) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: subAdditionalComponents!.length,
      itemBuilder: (_, index) {
        final SubAdditionalComponents subAdditionalComponent =
            subAdditionalComponents[index];

        final bool isSelected = subAdditionalCompnentManager
            .isSubAdditionalComponentExist(subAdditionalComponent.id!.toInt());

        return buildSubAdditionalComponentItem(
            subAdditionalComponent, numberOfSelect, isSelected, setState);
      },
    );
  }

  Widget buildSubAdditionalComponentItem(
      SubAdditionalComponents subAdditionalComponents,
      numberOfSelect,
      bool isSelected,
      StateSetter setState) {
    String price = (subAdditionalComponents.price! > 0)
        ? '${subAdditionalComponents.price!} SAR'
        : '';

    return Padding(
      padding: const EdgeInsets.only(bottom: 5, top: 5),
      child: PhysicalModel(
        color: Colors.white,
        elevation: 5,
        shadowColor: Colors.white,
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          onTap: () {},
          child: ClipRRect(
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(0),
                  decoration: const BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(4.0),
                      bottomLeft: Radius.circular(4.0),
                    ),
                  ),
                  width: 20,
                  height: 73,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Center(
                    child: ListTile(
                      title: textItem(
                        title: subAdditionalComponents.componentName.toString(),
                        value: price,
                        unit: "",
                      ),
                      onTap: () {},
                      leading: GlowCheckbox(
                        value: isSelected,
                        enable: true,
                        color: Colors.green,
                        onChange: (bool value) {
                          setState(() {
                            if (value) {
                              if (subAdditionalCompnentManager.isSum(
                                      subAdditionalComponents
                                          .subAdditionalComponentTitleId) <
                                  numberOfSelect) {
                                subAdditionalCompnentManager
                                    .subAddAdditionalComponent(
                                        subAdditionalComponents.price!,
                                        1,
                                        subAdditionalComponents.id!,
                                        subAdditionalComponents
                                            .subAdditionalComponentTitleId!);
                              } else {
                                subAdditionalCompnentManager
                                    .removeSubAddAdditionalComponent(
                                        subAdditionalComponents.id!);
                              }
                            } else {
                              subAdditionalCompnentManager
                                  .removeSubAddAdditionalComponent(
                                      subAdditionalComponents.id!);
                            }
                            sumPriceTotal();
                          });
                        },
                      ),
                    ),
                  ),
                ),
                //const Icon(Icons.arrow_forward_ios, color: Colors.blue),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void addToCart(productDetails, totalPrice, int quantityCount, setState) {
    final additionalComponentJson = additionalCompnentManager.toJson();
    final subAdditionalComponentJson = subAdditionalCompnentManager.toJson();

    final cartProvider = Provider.of<CartProvider>(context, listen: false);

    List<Map<String, dynamic>> additionalComponentList =
        additionalComponentJson; // قد تحتاج لتحديد النوع الصحيح إذا لزم الأمر
    List<Map<String, dynamic>> subAdditionalComponentList =
        subAdditionalComponentJson; // قد تحتاج لتحديد النوع الصحيح إذا لزم الأمر

    List<Map<String, dynamic>> additionalCompnentCarts = [];
    List<Map<String, dynamic>> subAdditionalCompnentCarts = [];

    for (var item in additionalComponentList) {
      Map<String, dynamic> data = {
        "quantity": item["quantity"],
        "additionalCompnentCartId": item["additionalCompnentCartId"],
      };
      additionalCompnentCarts.add(data);
    }

    for (var item in subAdditionalComponentList) {
      Map<String, dynamic> data = {
        "quantity": item["quantity"],
        "subAdditionalCompnentCartId": item["subAdditionalCompnentCartId"],
      };
      subAdditionalCompnentCarts.add(data);
    }

    final cartItem = Cart(
      images: productDetails.coverImage,
      title: productDetails.title,
      quantity: quantityCount,
      price: totalPrice,
      productDetailsSizeId: productDetailsSizesId!.toInt(),
      additionalComponentCarts: additionalCompnentCarts.map((item) {
        return AdditionalComponentCart(
          quantity: item["quantity"],
          additionalCompnentCartId: item["additionalCompnentCartId"],
        );
      }).toList(),
      subAdditionalComponentCarts: subAdditionalCompnentCarts.map((item) {
        return SubAdditionalComponentCart(
          quantity: item["quantity"],
          subAdditionalCompnentCartId: item["subAdditionalCompnentCartId"],
        );
      }).toList(),
    );


    if ((additionalComponentTitle == null ||
            checkDataEqualityForList(
                additionalComponentTitle!, additionalComponentJson)) &&
        productDetailsSizesId != 0) {
      cartProvider.addToCart(cartItem);
      setState(() {
        Navigator.of(context).pop();
      });
    } else {
      Toast.motionToastError(
          context, 'البيانات غير متطابقة أو لا تحتوي على الشروط المطلوبة.');
    }
  }

  bool checkDataEqualityForList(
      List<AdditionalComponentTitles> data1, List<Map<String, dynamic>> data2) {
    for (final item in data1) {
      if (item.isRequired == true) {
        int itemCount = data2
            .where((map) => map['additionalComponentTitleId'] == item.id)
            .length;

        if (!data2.any((map) => map['additionalComponentTitleId'] == item.id) &&
            itemCount != item.numberOfSelect) {
          return false;
        }
      }
    }

    return true;
  }
}

class AdditionalComponentManager {
  List<Map<String, dynamic>> additionalCompnentCarts = [];

  void addAdditionalComponent(price, int quantity, int additionalCompnentCartId,
      int additionalComponentTitleId) {
    Map<String, dynamic> newAdditionalComponent = {
      "price": price,
      "quantity": quantity,
      "additionalCompnentCartId": additionalCompnentCartId,
      "additionalComponentTitleId": additionalComponentTitleId,
    };

    additionalCompnentCarts.add(newAdditionalComponent);
  }

  void removeAdditionalComponent(int additionalCompnentCartId) {
    additionalCompnentCarts.removeWhere((element) =>
        element['additionalCompnentCartId'] == additionalCompnentCartId);
  }

  void updateAdditionalComponent(
      int additionalCompnentCartId, int newQuantity) {
    final index = additionalCompnentCarts.indexWhere((element) =>
        element['additionalCompnentCartId'] == additionalCompnentCartId);

    if (index != -1) {
      additionalCompnentCarts[index]['quantity'] = newQuantity;
    }
  }

  bool isAdditionalComponentExist(int additionalCompnentCartId) {
    return additionalCompnentCarts.any((element) =>
        element['additionalCompnentCartId'] == additionalCompnentCartId);
  }

  int isSum(additionalComponentTitleId) {
    return additionalCompnentCarts
        .where((element) =>
            element['additionalComponentTitleId'] == additionalComponentTitleId)
        .length;
  }

  double sumPrice() {
    double total = 0.0;
    for (var component in additionalCompnentCarts) {
      total += component['price'] * component['quantity'];
    }
    return total;
  }

  List<Map<String, dynamic>> toJson() {
    return additionalCompnentCarts;
  }
}

class SubAdditionalComponentManager {
  List<Map<String, dynamic>> subAdditionalCompnentCarts = [];

  void subAddAdditionalComponent(price, int quantity,
      int subAdditionalCompnentCartId, int subAdditionalComponentTitleId) {
    Map<String, dynamic> newAdditionalComponent = {
      "price": price,
      "quantity": quantity,
      "subAdditionalCompnentCartId": subAdditionalCompnentCartId,
      "subAdditionalComponentTitleId": subAdditionalComponentTitleId,
    };
    subAdditionalCompnentCarts.add(newAdditionalComponent);
  }

  void removeSubAddAdditionalComponent(int subAdditionalCompnentCartId) {
    subAdditionalCompnentCarts.removeWhere((element) =>
        element['subAdditionalCompnentCartId'] == subAdditionalCompnentCartId);
  }

  void updateSubAdditionalComponent(
      int subAdditionalCompnentCartId, int newQuantity) {
    final index = subAdditionalCompnentCarts.indexWhere((element) =>
        element['subAdditionalCompnentCartId'] == subAdditionalCompnentCartId);

    if (index != -1) {
      subAdditionalCompnentCarts[index]['quantity'] = newQuantity;
    }
  }

  bool isSubAdditionalComponentExist(int subAdditionalCompnentCartId) {
    return subAdditionalCompnentCarts.any((element) =>
        element['subAdditionalCompnentCartId'] == subAdditionalCompnentCartId);
  }

  int isSum(subAdditionalComponentTitleId) {
    return subAdditionalCompnentCarts
        .where((element) =>
            element['subAdditionalComponentTitleId'] ==
            subAdditionalComponentTitleId)
        .length;
  }

  double sumPrice() {
    double total = 0.0;
    for (var component in subAdditionalCompnentCarts) {
      total += component['price'] * component['quantity'];
    }
    return total;
  }

  List<Map<String, dynamic>> toJson() {
    return subAdditionalCompnentCarts;
  }
}
