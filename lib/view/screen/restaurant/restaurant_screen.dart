import 'dart:async';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food/components/cart_provider.dart';
import 'package:food/constans/colors.dart';
import 'package:food/controller/shop_controller.dart';
import 'package:food/model/address/ApproxPrice.dart';
import 'package:food/model/shop/ShopMenue.dart';
import 'package:food/model/shop/ShopSearch.dart';
import 'package:food/view/screen/cart/cart_screen.dart';
import 'package:food/view/screen/order/order_details_screen.dart';
import 'package:food/widgets/shop/categories_list.dart';
import 'package:food/view/screen/restaurant/components/product_list.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:redacted/redacted.dart';

class RestaurantScreen extends StatefulWidget {
  final Data data; // ShopSearch Model

  const RestaurantScreen(this.data, {super.key});

  @override
  State<RestaurantScreen> createState() => _ProductItemScreen();
}

class _ProductItemScreen extends State<RestaurantScreen> {
  final _shopController = Get.put(ShopController());

  late Future<ShopMenue> listShopMenue;
  late Future<ApproxPrice> approxPrice;

  var selected = 0;

  final pageController = PageController();

  final int countItem = 0;
  NumberFormat formatter = NumberFormat("0.00");

  late CartProvider cartProvider;

  @override
  void initState() {
    super.initState();

    listShopMenue = _shopController.getShopMenue(widget.data.id!.toString());
    approxPrice = _shopController.getApproxPrice(widget.data.shopBrnachId!);

    cartProvider = Provider.of<CartProvider>(context, listen: false);
    cartProvider.clearCart();
  }

  @override
  Widget build(BuildContext context) {
    Visibility(
      visible: cartProvider.cartItems.isNotEmpty,
      child: completeOrder(),
    );
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      CachedNetworkImage(
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width,
                        height: 150,
                        alignment: Alignment.topCenter,
                        imageUrl: widget.data.headerImage.toString(),
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) {
                          return Center(
                            child: SizedBox(
                              height: 15,
                              width: 15,
                              child: CircularProgressIndicator(
                                value: downloadProgress.progress,
                              ),
                            ),
                          );
                        },
                        errorWidget: (context, url, error) {
                          return const Icon(Icons.error);
                        },
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Container(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CachedNetworkImage(
                                        height: 40,
                                        width: 40,
                                        fit: BoxFit.fill,
                                        imageUrl: widget.data.avatar!,
                                        progressIndicatorBuilder:
                                            (context, url, downloadProgress) {
                                          return Center(
                                            child: SizedBox(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                  value: downloadProgress
                                                      .progress),
                                            ),
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const Icon(Icons.error);
                                        },
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        widget.data.shopName!,
                                        style: const TextStyle(
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 6,
                                      ),
                                      Text(
                                        widget.data.descriptionLocation!,
                                        style: const TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  const Icon(Icons.star, color: Colors.amber),
                                  Text(
                                    widget.data.shopeRate!.toString(),
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const Divider(),
                          Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: FutureBuilder<ApproxPrice>(
                                future: approxPrice,
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return const Text(
                                      "",
                                    ).redacted(context: context, redact: true);
                                  } else if (snapshot.hasError) {
                                    // Handle the error state, you can display an error message
                                    return Text('Error: ${snapshot.error}');
                                  } else if (!snapshot.hasData) {
                                    // Handle the case where the future completed with no data
                                    return const Text('No data available');
                                  } else {
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        _informationItem(
                                          title: "Distance",
                                          value: formatter
                                              .format(widget.data.distance!),
                                          unit: " km",
                                        ),
                                        const SizedBox(
                                          height: 30,
                                          child: VerticalDivider(
                                            color: Colors.red,
                                          ),
                                        ),
                                        _informationItem(
                                          title: "Delivery Price",
                                          value: formatter.format(snapshot
                                              .data!.returnData!.distance),
                                          unit: " SR",
                                        ),
                                        const SizedBox(
                                          height: 30,
                                          child: VerticalDivider(
                                            color: Colors.red,
                                          ),
                                        ),
                                        _informationItem(
                                          title: "Minimum Order",
                                          value: formatter.format(snapshot
                                              .data!.returnData!.priceFrom),
                                          unit: " SR",
                                        ),
                                      ],
                                    );
                                  }
                                }),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                buttonArrow(context),
                scroll(),
                Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Consumer<CartProvider>(
                      builder: (context, cartProvider, child) {
                        return Visibility(
                          visible: cartProvider.cartItems.isNotEmpty,
                          child: completeOrder(),
                        );
                      },
                    ))
              ],
            ),
          ),
        ],
      ),
    ));
  }

  buttonArrow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              clipBehavior: Clip.hardEdge,
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 30, sigmaY: 30),
                child: Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: const Icon(
                    Icons.arrow_back_ios,
                    size: 20,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: InkWell(
            onTap: () {
              Get.to((const CartScreen()));
            },
            child: Container(
              clipBehavior: Clip.hardEdge,
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: const Icon(
                    Icons.shopping_cart_outlined,
                    size: 20,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  scroll() {
    return DraggableScrollableSheet(
      initialChildSize: 0.65,
      maxChildSize: 0.9,
      minChildSize: 0.6,
      builder: (context, scrollController) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 60,
                child: CategoriesList(selected, (int index) {
                  setState(() {
                    selected = index;
                  });
                  pageController.jumpToPage(index);
                }, listShopMenue),
              ),
              Expanded(
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: ProductList(selected, (int index) {
                      setState(() {
                        selected = index;
                      });
                    }, pageController, listShopMenue),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _informationItem(
      {required String title, required String value, required String unit}) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              value,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
            Text(
              unit,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
          ],
        ),
        Text(
          title,
          style: const TextStyle(
            fontSize: 12,
            color: Colors.black38,
          ),
        ),
      ],
    );
  }

  completeOrder() {
    return InkWell(
      onTap: () {
        Get.to(const OrderDetailsScreen());
      },
      child: Container(
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
          color: primary,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child:  Consumer<CartProvider>(
          builder: (BuildContext context, CartProvider value, Widget? child) {
            double sumPrice = cartProvider.sumPrice();

           return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "${cartProvider.cartItems.length} items",
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                color: form,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.all(3.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Complete order",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: form,
                                fontSize: 16,
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios_outlined,
                              size: 16,
                              color: form,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                   Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Text(
                      "$sumPrice SAR",
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        color: form,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },

        ),
      ),
    );
  }
}
