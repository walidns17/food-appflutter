import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:food/components/constants.dart';
import 'package:food/constans/colors.dart';
import 'package:food/model/cart_Item.dart';
import 'package:provider/provider.dart';

import '../../../components/cart_provider.dart';

class OrderDetailsScreen extends StatefulWidget {
  const OrderDetailsScreen({super.key});

  @override
  State<OrderDetailsScreen> createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  int quantityCount = 1;

  void decrementQuantity() {
    setState(() {
      if (quantityCount > 1) {
        quantityCount--;
      }
    });
  }

  void incrementQuantity() {
    setState(() {
      quantityCount++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Sign In"),
          ],
        ),
      ),
      body: body(),
    );
  }

  body() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            Container(
              child: _addressDetails(),
            ),
            // Container(
            //   child: _paymentTypes(),
            // ),

            Container(
              child: _orderItems(),
            ),
          ],
        ),
      ),
    );
  }

  _addressDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 15,
        ),
        const Text(
          'Address details',
          style: TextStyle(
            fontSize: 16,
            color: Color(0XFF9D9D9D),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Container(
          padding: const EdgeInsets.only(top: 13, bottom: 13),
          decoration: const BoxDecoration(
              color: myCustomGreyColor,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(flex: 1, child: SvgPicture.asset("assets/svg/box.svg")),
              const Expanded(
                flex: 3,
                child: Text(
                  'Select your Location',
                  style: TextStyle(
                    fontSize: 16,
                    color: primary79,
                  ),
                ),
              ),
              const Expanded(
                  flex: 1,
                  child: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: SecondaryText,
                  )),
            ],
          ),
        ),
      ],
    );
  }

  _paymentTypes() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 25,
        ),
        const Text(
          'Payment Types',
          style: TextStyle(
            fontSize: 16,
            color: Color(0XFF9D9D9D),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Container(
          padding: const EdgeInsets.only(top: 13, bottom: 13),
          decoration: const BoxDecoration(
              color: myCustomGreyColor,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                  flex: 1,
                  child: SizedBox(
                      height: 30,
                      child: SvgPicture.asset(
                        "assets/svg/credit_card.svg",
                      ))),
              const Expanded(
                flex: 3,
                child: Text(
                  'mada Debit Card, Credit Card',
                  style: TextStyle(
                    fontSize: 16,
                    color: mainText,
                  ),
                ),
              ),
              const Expanded(
                  flex: 1,
                  child: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: SecondaryText,
                  )),
            ],
          ),
        ),
      ],
    );
  }

  _orderItems() {
    final cartProvider = Provider.of<CartProvider>(context, listen: false);
    List<Cart> cartItems = cartProvider.cartItems;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 25,
        ),
        const Text(
          'Order Items',
          style: TextStyle(
            fontSize: 16,
            color: Color(0XFF9D9D9D),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Container(
          width: double.maxFinite,
          padding: const EdgeInsets.only(top: 13, bottom: 13),
          decoration: const BoxDecoration(
              color: myCustomGreyColor,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: ListView.builder(
            shrinkWrap: true,
            physics: const PageScrollPhysics(),
            itemCount: (cartItems.isNotEmpty) ? (cartItems.length) : 0,
            itemBuilder: (_, index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    flex: 1,
                    child: AspectRatio(
                      aspectRatio: 0.88,
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: const Color(0xFFF5F6F9),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: CachedNetworkImage(
                            fit: BoxFit.fitHeight,
                            height: 200,
                            imageUrl: cartItems[index].images.toString(),
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) {
                              return Center(
                                child: SizedBox(
                                  height: 15,
                                  width: 15,
                                  child: CircularProgressIndicator(
                                    value: downloadProgress.progress,
                                  ),
                                ),
                              );
                            },
                            errorWidget: (context, url, error) {
                              return const Icon(Icons.error);
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          cartItems[index].title,
                          style: const TextStyle(
                              color: Colors.black, fontSize: 16),
                          maxLines: 2,
                        ),
                        const SizedBox(height: 10),
                        Text.rich(
                          TextSpan(
                            text: "SAR ${cartItems[index].price}",
                            style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                color: kPrimaryColor),
                            children: [
                              TextSpan(
                                  text: " x${cartItems[index].quantity}",
                                  style: Theme.of(context).textTheme.bodyText1),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        Center(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                              child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    decrementQuantity();
                                  });
                                },
                                icon: const Icon(
                                  Icons.delete,
                                  color: SecondaryText,
                                  size: 20,
                                ),
                              ),
                            ),
                            SizedBox(
                              child: Center(
                                child: Text(
                                  cartItems[index].quantity.toString(),
                                  style: const TextStyle(
                                      color: mainText,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                              child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      incrementQuantity();
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.add,
                                    color: SecondaryText,
                                    size: 20,
                                  )),
                            ),
                          ],
                        )),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}
