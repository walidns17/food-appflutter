import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:food/constans/colors.dart';

class NotFound404Error extends StatelessWidget {
  const NotFound404Error({Key? key, required this.text, required this.image}) : super(key: key);

  final String text;
  final String image;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        width: double.maxFinite,
        alignment: Alignment.center,
        child: Column(

          children: [
            const SizedBox(height: 100,),
            SvgPicture.asset(
              image,
              fit: BoxFit.fill,
              height: 100,
              width: 100,
            ),
            const SizedBox(height: 10,),

            Text(text,
              style: const TextStyle(fontSize: 20,color: mainText),
            ),

          ],
        ),
      ),
    );
  }
}
