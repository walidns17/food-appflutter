import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color primary = Color(0xFF0DAE91);
const Color primary79 = Color(0xFF0DAE91);

const Color Secondary = Color(0xFFFF6464);
const Color mainText = Color(0xFF2E3E5C);
const Color SecondaryText = Color(0xFF9FA5C0);
const Color outline = Color(0xFFD0DBEA);
const Color form = Color(0xFFF4F5F7);

const Color myCustomColor = Color(0xFFAABBCC); // 0xFF تحدد الشفافية، و AABBCC تحدد اللون.
const Color myCustomGreyColor = Color.fromRGBO(245, 245, 245, 1.0);

// Here we created a special class for the colors used in the project for ease of use later