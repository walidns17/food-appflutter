import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FrostedAppBar extends StatelessWidget {
  final String title;
  final List<Widget> actions;
  final bool showLeading;

  const FrostedAppBar(
      {required this.title,required this.actions, this.showLeading = false})
      ;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 100,
      automaticallyImplyLeading: showLeading,
      backgroundColor: Colors.transparent,
      pinned: true,
      floating: false,
      snap: false,
      actions: actions ?? [],
      flexibleSpace: Container(
        child: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
            child: FlexibleSpaceBar(
              titlePadding: const EdgeInsets.all(8.0),
              title: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    );
  }
}