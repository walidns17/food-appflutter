import 'package:flutter/material.dart';
import 'package:food/widgets/_build_back_button.dart';


class HeadWidget extends StatelessWidget {
  const HeadWidget({Key? key, required this.text, this.userAddressController, this.listOfUserAddress, this.userAddress})
      : super(key: key);


  final String text;
  final userAddressController;
  final listOfUserAddress;
  final userAddress;


  @override
  Widget build(BuildContext context) {

    return Row(
      children: <Widget>[
        const Expanded(
          flex: 1,
          child: BuildBackButton(),
        ),
        Expanded(
          flex: 7,
          child: _buildTitleAndLocation(text,context),
        ),
      ],
    );


  }

  Widget _buildTitleAndLocation(text,context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(5, 50, 5, 5),
      child: Column(
        children: [
          Text(
            text.toString(),
            style: const TextStyle(fontSize: 20),
          ),
          GestureDetector(
            onTap: () {

              userAddressController.showAddressDialog(
                    listOfUserAddress, userAddress, context);

            },
            child: _buildLocationInfo(),
          ),
        ],
      ),
    );
  }

  Widget _buildLocationInfo() {
    return Container(
      padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildLocationText(),
          const Center(child: Icon(Icons.arrow_drop_down)),
        ],
      ),
    );
  }

  Widget _buildLocationText() {
    return FutureBuilder<String>(
      future: Future.delayed(const Duration(seconds: 1), () {
        return userAddressController.getAddressLocation();
      }),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: SizedBox(
              height: 15,
              width: 15,
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          return RichText(
            overflow: TextOverflow.ellipsis,
            strutStyle: const StrutStyle(fontSize: 12.0),
            text: TextSpan(
              children: [
                const TextSpan(
                  text: ' Deliver to ',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                  ),
                ),
                TextSpan(
                  text: ' ${snapshot.data} ',
                  style: const TextStyle(
                    color: Color(0xFF64C8B6),
                    fontSize: 13,
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
