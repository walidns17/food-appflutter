import 'dart:async';

import 'package:flutter/material.dart';
import 'package:food/controller/user_address_controoler.dart';
import 'package:food/model/address/UserAddress.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';


class ListOfUserAddress extends StatelessWidget {
  ListOfUserAddress({Key? key, required this.listOfUserAddress})
      : super(key: key);

  Future<UserAddress> listOfUserAddress;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: SizedBox(
            child: FutureBuilder<UserAddress>(
                future: listOfUserAddress,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    // Return a loading indicator or placeholder widget
                    return const Center(
                        child: SizedBox(
                            height: 50,
                            width: 50,
                            child: CircularProgressIndicator()));
                  } else if (snapshot.hasError) {
                    // Handle the error state, you can display an error message
                    return Text('Error: ${snapshot.error}');
                  } else if (!snapshot.hasData) {
                    // Handle the case where the future completed with no data
                    return const Text('No data available');
                  } else {
                    return ListView.builder(
                        physics: const PageScrollPhysics(),
                        itemCount: snapshot.data!.returnData!.data!.length,
                        itemBuilder: (_, index) {
                          var product =
                              snapshot.data!.returnData!.data!.toList();
                          return SizedBox(
                            width: 400,
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(

                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.greenAccent),
                                    borderRadius: BorderRadius.circular(20),
                                  ),

                                  /** CheckboxListTile Widget **/
                                  child: FutureBuilder<bool>(
                                    future: checkSelectedLocation(
                                        product[index].id.toString()),
                                    builder: (context, snapshot) {
                                      return CheckboxListTile(
                                        title: Text(product[index]
                                            .descriptionLocation
                                            .toString()),
                                        subtitle: Text(
                                            product[index].address.toString()),
                                        secondary:
                                            const Icon(Icons.share_location),
                                        autofocus: false,
                                        activeColor: Colors.green,
                                        checkColor: Colors.white,
                                        value: snapshot.data == false,
                                        onChanged: (value) {
                                          _saveSelectedLocation(
                                              product[index].id.toString(),
                                              product[index].descriptionLocation.toString());

                                          Navigator.pop(context);
                                        },
                                      );
                                    },
                                  ), //CheckboxListTile
                                ), //Container
                              ), //Padding
                            ), //Center
                          );
                        });
                  }
                }),
          ),
        ),

    );
  }

  Future<void> _saveSelectedLocation(String location,String descriptionLocation) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('selected_location', location);
    await prefs.setString('description_location', descriptionLocation);

  }

  Future<bool> checkSelectedLocation(String i) async {
    final prefs = await SharedPreferences.getInstance();
    final location = prefs.getString('selected_location');

    return location != i;
  }
}
