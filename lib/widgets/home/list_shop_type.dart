import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food/model/home/ShopType.dart';
import 'package:food/view/home/shop/shop_list_page.dart';
import 'package:get/get.dart';


class ListShopType extends StatelessWidget {
  ListShopType({Key? key, required this.listOfShopType})
      : super(key: key);

  Future<ShopType> listOfShopType;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Expanded(
        child: FutureBuilder<ShopType>(
            future: listOfShopType,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                // Return a loading indicator or placeholder widget
                return const Center(
                    child: SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator()));
              } else if (snapshot.hasError) {
                // Handle the error state, you can display an error message
                return Text('Error: ${snapshot.error}');
              } else if (!snapshot.hasData) {
                // Handle the case where the future completed with no data
                return const Text('No data available');
              } else {
                return GridView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: snapshot.data!.returnData!
                        .where((e) => e.isActive == true)
                        .toList()
                        .length,
                    gridDelegate:
                    const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      mainAxisSpacing: 2.0,
                      crossAxisSpacing: 2.0,
                      childAspectRatio: 1,
                    ),
                    itemBuilder: (_, index) {
                      var product = snapshot.data!.returnData!
                          .where((e) => e.isActive == true)
                          .toList();
                      return InkWell(
                        onTap: () {
                          Get.to((ShopListPage(id: product[index].id.toString()
                              ,foodTypeName: product[index].description.toString())));

                        } ,

                        child: Center(
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Color(0xFFF2F2F2),
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CachedNetworkImage(
                                      height: 60,
                                      width: 70,
                                      fit: BoxFit.cover,
                                      imageUrl: product[index].shopTypeAvatar.toString(),
                                      progressIndicatorBuilder: (context, url,
                                          downloadProgress) =>
                                          CircularProgressIndicator(
                                              value: downloadProgress.progress),
                                      errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 2,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    product[index].type.toString(),
                                    style: const TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              }
            }),
      ),
    );
  }

}
