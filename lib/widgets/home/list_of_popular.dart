import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food/model/home/Popular.dart';


class ListOfPopular extends StatelessWidget {
  ListOfPopular({Key? key, required this.listOfPopular})
      : super(key: key);

  Future<Popular> listOfPopular;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          const Divider(),
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Container(
              alignment: Alignment.topLeft,
              child: const Text("Trending Now",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: SizedBox(
              height: double.maxFinite,
              child: FutureBuilder<Popular>(
                  future: listOfPopular,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      // Return a loading indicator or placeholder widget
                      return const Center(
                          child: SizedBox(
                              height: 50,
                              width: 50,
                              child: CircularProgressIndicator()));
                    } else if (snapshot.hasError) {
                      // Handle the error state, you can display an error message
                      return Text('Error: ${snapshot.error}');
                    } else if (!snapshot.hasData) {
                      // Handle the case where the future completed with no data
                      return const Text('No data available');
                    } else {
                      return ListView.builder(
                          physics: const PageScrollPhysics(),
                          itemCount: snapshot.data!.returnData!.length,
                          itemBuilder: (_, index) {
                            var product = snapshot.data!.returnData!.toList();
                            return Container(
                              height: 250,
                              decoration: const BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: ClipRRect(
                                        borderRadius:
                                        BorderRadius.circular(16.0),
                                        child: CachedNetworkImage(
                                          width: double.maxFinite,
                                          fit: BoxFit.cover,
                                          imageUrl: product[index]
                                              .headerImage
                                              .toString(),
                                          progressIndicatorBuilder: (context,
                                              url, downloadProgress) =>
                                              CircularProgressIndicator(
                                                  value: downloadProgress
                                                      .progress),
                                          errorWidget: (context, url, error) =>
                                          const Icon(Icons.error),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Center(
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            height: 30,
                                            width: 30,
                                            child: ClipRRect(
                                              borderRadius:
                                              BorderRadius.circular(16.0),
                                              child: CachedNetworkImage(
                                                width: double.maxFinite,
                                                fit: BoxFit.cover,
                                                imageUrl: product[index]
                                                    .avatar
                                                    .toString(),
                                                progressIndicatorBuilder:
                                                    (context, url,
                                                    downloadProgress) =>
                                                    CircularProgressIndicator(
                                                        value:
                                                        downloadProgress
                                                            .progress),
                                                errorWidget:
                                                    (context, url, error) =>
                                                const Icon(Icons.error),
                                              ),
                                            ),
                                          ),
                                          Column(
                                            children: [
                                              Padding(
                                                padding:
                                                const EdgeInsets.fromLTRB(
                                                    10, 4, 10, 10),
                                                child: Text(
                                                    product[index]
                                                        .shopName
                                                        .toString(),
                                                    style: const TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                    )),
                                              ),
                                              Padding(
                                                padding:
                                                const EdgeInsets.fromLTRB(
                                                    10, 4, 10, 10),
                                                child: Text(
                                                    product[index]
                                                        .shopDescription
                                                        .toString(),
                                                    style: const TextStyle(
                                                      color: Color(0xFF3C3F41),
                                                      fontSize: 14,
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Divider(),
                                  ],
                                ),
                              ),
                            );
                          });
                    }
                  }),
            ),
          ),
        ],
      ),
    );
  }

}
