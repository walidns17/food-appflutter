import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:redacted/redacted.dart';


class ListOfBanner extends StatelessWidget {
  ListOfBanner({Key? key, required this.listOfBanner})
      : super(key: key);

  Future<List> listOfBanner;

  @override
  Widget build(BuildContext context) {
    return  Expanded(
      child: ListView(
        children: <Widget>[
          Center(
            child: FutureBuilder<List?>(
                future: listOfBanner,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Container(
                        height: 150,
                        width: double.maxFinite,
                        margin: const EdgeInsets.only(
                            right: 30, left: 30, top: 5),
                        padding: const EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: Colors.yellow[200],
                            border: Border.all(
                              color: Colors.green,
                              width: 20,
                            )),
                        child: const Text(''),
                      ).redacted(
                        context: context,
                        redact: true,
                      );
                    default:
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        List data = snapshot.data ?? [];
                        return CarouselSlider.builder(
                          itemBuilder: (context, index, realIndex) {
                            return Container(
                              margin: const EdgeInsets.all(5.0),
                              width: double.maxFinite,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(8.0),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      data[index]['imagePath']),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                          options: CarouselOptions(
                            height: 150,
                            enlargeCenterPage: true,
                            autoPlay: true,
                            aspectRatio: 16 / 9,
                            autoPlayCurve: Curves.fastOutSlowIn,
                            enableInfiniteScroll: true,
                            autoPlayAnimationDuration:
                            const Duration(milliseconds: 2000),
                            viewportFraction: 0.8,
                          ),
                          itemCount: data.length,
                        );
                      }
                  }
                }),
          ),
        ],
      ),
    );
  }

}
