import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:food/model/shop/ShopMenue.dart';
import 'package:flutter/material.dart';
import 'package:redacted/redacted.dart';

class CategoriesList extends StatelessWidget {
  late final int selected;
  late final Function callback;
  late Future<ShopMenue> listShopMenue;

  CategoriesList(this.selected, this.callback, this.listShopMenue);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: FutureBuilder<ShopMenue>(
          future: listShopMenue,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                  height: 110,
                  color: Colors.white12,
                  margin: const EdgeInsets.all(5),
                  child: ListView.builder(
                    itemCount: 4,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext context, int index) {
                      return const Text(
                        "",
                      );
                    },
                  )).redacted(context: context, redact: true);
            } else if (snapshot.hasError) {
              // Handle the error state, you can display an error message
              return Text('Error: ${snapshot.error}');
            } else if (!snapshot.hasData) {
              // Handle the case where the future completed with no data
              return const Text('No data available');
            } else {
              return SizedBox(
                child: ListView.separated(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (_, categoryIndex) {
                    var category = snapshot.data!.returnData![categoryIndex];

                    return GestureDetector(
                      onTap: () => callback(categoryIndex),
                      child: Container(
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: selected == categoryIndex
                              ? Colors.green
                              : Colors.white,
                        ),
                        child: Text(
                          category.productName!,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: selected == categoryIndex
                                ? Colors.white
                                : Colors.black,
                          ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (_, index) => const SizedBox(width: 20),
                  itemCount: snapshot.data!.returnData!.length,
                ),
              );
            }
          }),
    );
  }
}
