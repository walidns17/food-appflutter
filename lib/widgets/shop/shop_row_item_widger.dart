import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food/model/shop/ShopSearch.dart';
import 'package:food/view/screen/restaurant/restaurant_screen.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

class ShopRowItemWidget extends StatelessWidget {

  final Data data; // ShopSearch Model
  const ShopRowItemWidget(this.data, {super.key});

  @override
  Widget build(BuildContext context) {
    NumberFormat formatter = NumberFormat("0.00");
    String myVariable = data.isOpen!
        ? "Open"
        : "Closed";

    return Container(
        margin: const EdgeInsets.all(5),
        child: PhysicalModel(
          color: Colors.white,
          elevation: 5,
          shadowColor: Colors.white,
          borderRadius: BorderRadius.circular(10),
          child: InkWell(
            onTap: () {
              Get.to(RestaurantScreen(data));
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: CachedNetworkImage(
                            height: 60,
                            width: 70,
                            fit: BoxFit.fill,
                            imageUrl: data.avatar!,
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) {
                              return Center(
                                child: SizedBox(
                                  height: 15,
                                  width: 15,
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress),
                                ),
                              );
                            },
                            errorWidget: (context, url, error) {
                              return const Icon(Icons.error);
                            },
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        padding: const EdgeInsets.all(14),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    const Icon(Icons.star, color: Colors.amber),
                                    Text(
                                      data.shopeRate!.toString(),
                                      style: const TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  data.shopName!,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                            RichText(
                              overflow: TextOverflow.ellipsis,
                              strutStyle: const StrutStyle(
                                fontSize: 12.0,
                              ),
                              text: TextSpan(
                                style: const TextStyle(
                                  color: Colors.black38,
                                ),
                                text: data.descriptionLocation!,
                              ),
                            ),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  myVariable,
                                  style: const TextStyle(
                                    color: Colors.green,
                                  ),
                                ),
                                const SizedBox(
                                  height: 30,
                                  child: VerticalDivider(
                                    color: Colors.red,
                                  ),
                                ),
                                Flexible(
                                  child: RichText(
                                    overflow: TextOverflow.ellipsis,
                                    strutStyle: const StrutStyle(
                                      fontSize: 12.0,
                                    ),
                                    text: TextSpan(
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                      text: formatter.format(data.distance!),
                                    ),
                                  ),
                                ),
                                const Text(
                                  "km",
                                  style: TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
