import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductInfoWidget extends StatelessWidget {
  final  productDetails;

  const ProductInfoWidget({super.key, required this.productDetails});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Container(
            padding: const EdgeInsets.all(8.0),

            child: CachedNetworkImage(

              fit: BoxFit.fitHeight,

              imageUrl: productDetails.coverImage,
              imageBuilder: (context, imageProvider) => Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              progressIndicatorBuilder: (context, url, downloadProgress) =>
                  Center(
                    child: SizedBox(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                          value: downloadProgress.progress),
                    ),
                  ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
        ),
        const SizedBox(height: 5,),
        Text(
          productDetails.title,
          style: const TextStyle(fontSize: 14, color: Colors.black),
        ),
        const SizedBox(height: 10,),
        Row(
          children: [
            Text(
              productDetails.price.toString(),
              style: const TextStyle(fontSize: 14, color: Colors.black),
            ),
            const Text("SR", style: TextStyle(fontSize: 14, color: Colors.black)),
          ],
        ),
      ],
    );
  }
}
