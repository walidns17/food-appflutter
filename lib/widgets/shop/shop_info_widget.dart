import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ShopInfoWidget extends StatelessWidget {
  final shop;

  const ShopInfoWidget({required this.shop});

  @override
  Widget build(BuildContext context) {
    NumberFormat formatter = NumberFormat("0.00");
    
    return Container(
      margin: const EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            shop.shopName,
            style: const TextStyle(fontSize: 16, color: Colors.black),
          ),
          const SizedBox(height: 10),
          Text(
            shop.shopBrnachDescriptionLocation,
            style: const TextStyle(fontSize: 16, color: Colors.black38),
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Flexible(
                child: RichText(
                  overflow: TextOverflow.ellipsis,
                  strutStyle: const StrutStyle(
                    fontSize: 12.0,
                  ),
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    text: formatter.format(shop.distance),
                  ),
                ),
              ),
              const Text("km", style: TextStyle(color: Colors.black)),
            ],
          ),
          const SizedBox(height: 15),

        ],
      ),
    );
  }
}
