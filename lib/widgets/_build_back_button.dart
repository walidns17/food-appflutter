import 'package:flutter/material.dart';

import '../utils/global.colors.dart';

class BuildBackButton extends StatelessWidget {
  const BuildBackButton({Key? key})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: const EdgeInsets.fromLTRB(5, 50, 5, 5),
      child: Stack(
        alignment: Alignment.topLeft,
        children: <Widget>[
          IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
