import 'package:flutter/material.dart';

class ButtonAdd extends StatelessWidget {
  final String text;
  final void Function()? onTop;

  const ButtonAdd({super.key, required this.text, this.onTop});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: const Color.fromARGB(100, 255, 255, 255),
          borderRadius: BorderRadius.circular(20)),
      padding: const EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            text,
            style: const TextStyle(color: Colors.white,fontSize: 20),
          ),
          const SizedBox(
            width: 10,
          ),
          const Icon(
            Icons.arrow_forward,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
