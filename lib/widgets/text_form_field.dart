import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class TextFormFieldWidget extends StatefulWidget {

  TextFormFieldWidget({Key? key, required this.maxLines, required this.hintText, required this.controller})
      : super(key: key);

  var maxLines, hintText;

  var controller = TextEditingController();

  @override
  State<TextFormFieldWidget> createState() => _TextFormFieldWidgetState();
}

class _TextFormFieldWidgetState extends State<TextFormFieldWidget> {

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Container(
          margin: const EdgeInsets.all(1),
          decoration: const BoxDecoration(
            color: Color(0xffF9F9F9),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                const SizedBox(width: 1),
                Expanded(
                  child: TextFormField(
                    controller: widget.controller,
                    maxLines:widget.maxLines,
                    decoration: InputDecoration(
                      hintText: widget.hintText,
                      border: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Colors.white12)),
                      disabledBorder: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Colors.white12)),
                      enabledBorder: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Colors.white12)),
                      errorBorder: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Colors.white12)),
                      focusedBorder: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Color(0XFF0DAE91))),
                      focusedErrorBorder: const OutlineInputBorder(
                          borderSide:
                          BorderSide(width: 1, color: Colors.white12)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
